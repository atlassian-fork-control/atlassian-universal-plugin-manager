package com.atlassian.upm.pageobjects;

import java.net.URI;

public class Link
{
    private final URI href;

    public Link(URI href)
    {
        this.href = href;
    }

    public URI getHref()
    {
        return href;
    }
}
