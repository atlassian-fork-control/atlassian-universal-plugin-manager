package com.atlassian.upm.pageobjects;

import java.util.Iterator;
import java.util.List;

import com.atlassian.webdriver.utils.element.ElementIsVisible;
import com.atlassian.webdriver.utils.element.ElementLocated;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.google.common.collect.Iterables.any;

public class Waits
{
    public static Function<WebDriver, Boolean> and(final Function<WebDriver, Boolean> p1, final Function<WebDriver, Boolean> p2)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return p1.apply(driver) && p2.apply(driver);
            }
        };
    }

    public static Function<WebDriver, Boolean> or(final Function<WebDriver, Boolean> p1, final Function<WebDriver, Boolean> p2)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return p1.apply(driver) || p2.apply(driver);
            }
        };
    }

    public static Function<WebDriver, Boolean> not(final Function<WebDriver, Boolean> p)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return !p.apply(driver);
            }
        };
    }

    public static Function<WebDriver, Boolean> selected(final WebElement tabPanel)
    {
        return hasClass(tabPanel, "upm-selected");
    }

    public static Function<WebDriver, Boolean> selectedAndLoaded(final WebElement tabPanel)
    {
        return and(selected(tabPanel), loaded(tabPanel));
    }

    public static Function<WebDriver, Boolean> loaded(WebElement e)
    {
        return hasClass(e, "loaded");
    }

    public static Function<WebDriver, Boolean> loading(WebElement e)
    {
        return hasClass(e, "loading");
    }

    public static Function<WebDriver, Boolean> anyAreVisible(final By selector)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                List<WebElement> elements = driver.findElements(selector);
                return any(elements, new Predicate<WebElement>()
                {
                    public boolean apply(WebElement input)
                    {
                        return input.isDisplayed();
                    }
                });
            }
        };
    }
    public static Function<WebDriver, Boolean> expanded(Iterable<WebElement> elements)
    {
        Function<WebDriver, Boolean> expanded = null;
        Iterator<WebElement> elementsIterator = elements.iterator();
        if (elementsIterator.hasNext())
        {
            expanded = hasClass(elementsIterator.next(), "expanded");
        }
        while (elementsIterator.hasNext())
        {
            expanded = and(expanded, hasClass(elementsIterator.next(), "expanded"));
        }
        return expanded;
    }

    public static Function<WebDriver, Boolean> collapsed(Iterable<WebElement> elements)
    {
        Function<WebDriver, Boolean> collapsed = null;
        Iterator<WebElement> elementsIterator = elements.iterator();
        if (elementsIterator.hasNext())
        {
            collapsed = doesNotHaveClass(elementsIterator.next(), "expanded");
        }
        while (elementsIterator.hasNext())
        {
            collapsed = and(collapsed, doesNotHaveClass(elementsIterator.next(), "expanded"));
        }
        return collapsed;
    }

    public static Function<WebDriver, Boolean> isDisabled(WebElement e)
    {
        return hasClass(e, "disabled");
    }

    public static Function<WebDriver, Boolean> isEnabled(WebElement e)
    {
        return not(isDisabled(e));
    }

    public static Function<WebDriver, Boolean> hasClass(final WebElement element, final String className)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver from)
            {
                return element.getAttribute("class").contains(className);
            }
        };
    }

    public static Function<WebDriver, Boolean> doesNotHaveClass(final WebElement element, final String className)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver from)
            {
                return !hasClass(element, className).apply(from);
            }
        };
    }

    public static Function<WebDriver, Boolean> hasTitle(final WebElement element, final String title)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return element.getAttribute("title").equals(title);
            }
        };
    }

    public static Function<WebDriver, Boolean> elementIsLocated(final By selector, final Supplier<WebElement> e)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return new ElementLocated(selector, e.get()).apply(driver);
            }
        };
    }

    public static Function<WebDriver, Boolean> elementIsVisible(final By selector, final Supplier<WebElement> e)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                try
                {
                    return new ElementIsVisible(selector, e.get()).apply(driver);
                }
                catch (StaleElementReferenceException e)
                {
                    return false;
                }
            }
        };
    }
}
