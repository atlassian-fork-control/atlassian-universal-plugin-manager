package com.atlassian.upm.fakepac;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.atlassian.plugins.rest.common.Status;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang.math.NumberUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.io.IOUtils.copy;
import static org.apache.commons.io.IOUtils.copyLarge;
import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * Serves up static files to imitate PAC.
 */
@Path("/")
@AnonymousAllowed
public class FakePacServices
{
    private static final String BASE_URL = System.getProperty("pac.baseurl") + "/1.0";
    private final Map<String, Long> pluginFileSizes = new HashMap<String, Long>();

    @GET
    @Path("pluginversion/find/compatible/{product-key}/{buildNumber}")
    @Produces(APPLICATION_XML)
    public String findPluginVersion(@QueryParam("expand") List<String> expand,
        @QueryParam("q") String query,
        @DefaultValue("0") @QueryParam("max-results") int maxResults,
        @DefaultValue("0") @QueryParam("start-index") int startIndex) throws Exception
    {
        checkContains(expand, "plugin");
        return filterContents("/find-plugin-version.xml", query, maxResults, startIndex);
    }

    @GET
    @Path("pluginversion/find/popular/{product-key}/{build-number}")
    @Produces(APPLICATION_XML)
    public String getPopularPlugins(@QueryParam("expand") List<String> expand,
        @DefaultValue("0") @QueryParam("max-results") int maxResults,
        @DefaultValue("0") @QueryParam("start-index") int startIndex) throws Exception
    {
        checkContains(expand, "plugin");
        return filterContents("/find-plugin-version.xml", maxResults, startIndex);
    }

    @GET
    @Path("pluginversion/find/supported/{product-key}/{build-number}")
    @Produces(APPLICATION_XML)
    public String getSupportedPlugins(@QueryParam("expand") List<String> expand,
        @DefaultValue("0") @QueryParam("max-results") int maxResults,
        @DefaultValue("0") @QueryParam("start-index") int startIndex) throws Exception
    {
        checkContains(expand, "plugin");
        return filterContents("/find-plugin-version.xml", maxResults, startIndex);
    }

    @GET
    @Path("pluginversion/find/featured/{product-key}/{buildNumber}")
    @Produces(APPLICATION_XML)
    public String findFeatured(@QueryParam("expand") List<String> expand,
        @DefaultValue("0") @QueryParam("max-results") int maxResults,
        @DefaultValue("0") @QueryParam("start-index") int startIndex) throws Exception
    {
        checkContains(expand, "plugin");
        return filterContents("/find-featured.xml", maxResults, startIndex);
    }

    @GET
    @Path("pluginversion/find/compatiblekey/{product-key}/{build-number}/{plugin-key}")
    @Produces(APPLICATION_XML)
    public String findCompatibleKey(@QueryParam("expand") List<String> expand,
        @PathParam("plugin-key") String pluginKey) throws Exception
    {
        checkContains(expand, "plugin.icon", "plugin.vendor", "license", "reviewSummary");
        if ("trigger-500".equals(pluginKey))
        {
            throw new WebApplicationException(Response.status(500).entity(Status.error().message("Triggered an internal error because you asked for it.").build()).build());
        }
        return filterContents("/find-compatible-" + pluginKey + ".xml");
    }

    @POST
    @Path("pluginversion/find/updates/{product-key}/{buildNumber}")
    @Consumes(APPLICATION_FORM_URLENCODED)
    @Produces(APPLICATION_XML)
    public String findUpdates(@QueryParam("expand") List<String> expand) throws Exception
    {
        checkContains(expand, "plugin");
        return filterContents("/find-updates.xml");
    }

    @GET
    @Path("product/after/{product-key}/{buildNumber}")
    @Produces(APPLICATION_XML)
    public String getProductUpgrades() throws Exception
    {
        return filterContents("/product-upgrades.xml");
    }

    @GET
    @Path("product/latest/{product-key}")
    @Produces(APPLICATION_XML)
    public String getProductLatest(@PathParam("product-key") String productKey) throws Exception
    {
        return getContents("/product-versions-" + productKey + ".xml");
    }

    @POST
    @Path("pluginversion/find/compatibilitystatus/{product-key}/{currentBuildNumber}/{targetBuildNumber}")
    @Produces(APPLICATION_XML)
    public String getProductUpgradePluginCompatibility(@FormParam("expand") List<String> expand) throws Exception
    {
        checkContains(expand, "plugin", "productCompatibilities.productCompatibility.maxVersion", "productCompatibilities.productCompatibility.minVersion", "productCompatibilities.productCompatibility.product");
        return filterContents("/product-upgrade-plugin-compatibility.xml");
    }

    @GET
    @Path("plugins/{plugin}")
    @Produces(APPLICATION_OCTET_STREAM)
    public Response servePlugin(@PathParam("plugin") String plugin) throws IOException
    {
        return Response.ok(newStreamingOutput("/" + plugin)).header(HttpHeaders.CONTENT_LENGTH, sizeOf("/" + plugin)).build();
    }

    @GET
    @Path("xmlplugins/{plugin}")
    @Produces(APPLICATION_XML)
    public String serveXmlPlugin(@PathParam("plugin") String xmlPlugin) throws IOException
    {
        if (xmlPlugin != null && !xmlPlugin.endsWith(".xml"))
        {
            xmlPlugin = xmlPlugin.concat(".xml");
        }
        return getContents("/" + xmlPlugin);
    }

    @GET
    @Path("nonplugin")
    @Produces(APPLICATION_OCTET_STREAM)
    public Response serveNonPlugin() throws IOException
    {
        return Response.ok(new ByteArrayInputStream("This is not a plugin.".getBytes())).build();
    }

    private void checkContains(Collection<String> actual, String... expected)
    {
        List<String> missing = new ArrayList<String>();
        for (String e : expected)
        {
            if (!actual.contains(e))
            {
                missing.add(e);
            }
        }
        if (!missing.isEmpty())
        {
            throw new WebApplicationException(Response.
                status(BAD_REQUEST).
                entity("For PAC integration to work the following expand parameters are expected but missing: " + missing).
                build());
        }
    }

    private Long sizeOf(String plugin) throws IOException
    {
        if (!pluginFileSizes.containsKey(plugin))
        {
            pluginFileSizes.put(plugin, calculateFileSize(plugin));
        }
        return pluginFileSizes.get(plugin);
    }

    private Long calculateFileSize(String plugin) throws IOException
    {
        InputStream in = null;
        try
        {
            in = getClass().getResourceAsStream("/" + plugin);
            return copyLarge(in, new NullOutputStream());
        }
        finally
        {
            closeQuietly(in);
        }
    }

    private StreamingOutput newStreamingOutput(final String fileName)
    {
        return new StreamingOutput()
        {
            public void write(OutputStream output) throws IOException, WebApplicationException
            {
                InputStream input = getClass().getResourceAsStream(fileName);
                if (input == null)
                {
                    throw new WebApplicationException(NOT_FOUND);
                }
                try
                {
                    copy(input, output);
                }
                finally
                {
                    closeQuietly(input);
                }
            }
        };
    }

    private String filterContents(String fileName) throws Exception
    {
        return filterContents(fileName, 0, 0);
    }

    private String filterContents(String fileName, int maxResults, int startIndex) throws Exception
    {
        return filterContents(fileName, null, maxResults, startIndex);
    }

    private String filterContents(String fileName, String query, int maxResults, int startIndex) throws Exception
    {
        String fileContents = getContents(fileName);

        if ((isEmpty(query) || "alias:Plugin".equals(query)) && maxResults == 0 && startIndex == 0)
        {
            return fileContents;
        }

        Document doc = parseXml(fileContents);

        filterSearchQuery(doc, query);
        filterPagination(doc, maxResults, startIndex);

        return getXmlString(doc);
    }

    private String getContents(String fileName) throws IOException
    {
        InputStream input = getClass().getResourceAsStream(fileName);
        if (input == null)
        {
            throw new WebApplicationException(NOT_FOUND);
        }
        try
        {
            String contents = IOUtils.toString(input);
            return contents.replaceAll("\\$\\{fakepac\\.baseurl\\}", BASE_URL);
        }
        finally
        {
            closeQuietly(input);
        }
    }

    private void filterSearchQuery(Document doc, String query) throws Exception
    {
        if (isEmpty(query) || "alias:Plugin".equals(query))
        {
            return;
        }

        Element items = (Element) doc.getFirstChild();
        int size = NumberUtils.createNumber(items.getAttribute("size")).intValue();
        int remaining = size;

        for (int i = size - 1; i > -1; i--)
        {
            Element item = (Element) items.getElementsByTagName("item").item(i);
            String title = item.getElementsByTagName("name").item(0).getTextContent();

            if (!title.toLowerCase().matches(".*" + query.toLowerCase() + ".*"))
            {
                items.removeChild(item);
                remaining--;
            }
        }

        // set the size attribute of the items tag
        items.setAttribute("size", String.valueOf(remaining));
    }

    private void filterPagination(Document doc, int maxResults, int startIndex) throws Exception
    {
        if (maxResults == 0 && startIndex == 0)
        {
            return;
        }

        Element items = (Element) doc.getFirstChild();
        int size = NumberUtils.createNumber(items.getAttribute("size")).intValue();
        int remaining = size;

        if (startIndex > 0)
        {
            for (int i = Math.min(startIndex, size) - 1; i > -1; i--)
            {
                Node item = items.getElementsByTagName("item").item(i);
                items.removeChild(item);
            }
            remaining = size - startIndex;
        }

        if (maxResults > 0 && remaining > maxResults)
        {
            for (int i = remaining - 1; i >= maxResults; i--)
            {
                Node item = items.getElementsByTagName("item").item(i);
                items.removeChild(item);
            }
            remaining = maxResults;
        }

        // set the size attribute of the items tag
        items.setAttribute("size", String.valueOf(remaining));
    }

    private Document parseXml(String xmlContent) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return factory.newDocumentBuilder().parse(new InputSource(new StringReader(xmlContent)));
    }

    private String getXmlString(Document doc) throws TransformerException
    {
        StreamResult xmlOutput = new StreamResult(new StringWriter());
        TransformerFactory tfactory = TransformerFactory.newInstance();
        Transformer serializer = tfactory.newTransformer();
        serializer.transform(new DOMSource(doc), xmlOutput);

        return xmlOutput.getWriter().toString();
    }
}
