package it.com.atlassian.upm.uitests;

import java.io.IOException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.pageobjects.UpgradeTab;
import com.atlassian.upm.pageobjects.UpgradeablePlugin;
import com.atlassian.upm.pageobjects.UpgradeablePluginDetails;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;

import com.google.inject.Inject;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.LEGACY_PLUGIN;
import static com.atlassian.upm.test.UpmTestGroups.CONFLUENCE;
import static com.atlassian.upm.pageobjects.UpgradeTab.Category.DEFAULT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class UpgradeTabTest
{
    private static final String UPM_PLUGIN_NAME = "Atlassian Universal Plugin Manager Plugin";
    
    private static @Inject TestedProduct<WebDriverTester> product;
    private static PluginManager upm;
    private UpgradeTab tab;

    @Rule public WebDriverScreenshotRule webDriverScreenshotRule = new WebDriverScreenshotRule();

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
    }

    @AfterClass
    public static void tearDown()
    {
        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Before
    public void goToUpgradeTab()
    {
        reloadPage();
        tab = upm.openUpgradeTab();
    }

    @Test
    public void installableTestPluginVisible()
    {
        assertTrue(tab.contains(INSTALLABLE.getKey()));
    }

    @Test
    public void upgradeAllExpandsPluginDetails() throws Exception
    {
        try
        {
            tab.upgradeAll();
            assertThat(tab.getPlugins(), everyItem(Matchers.<UpgradeablePlugin>either(hasPluginDetailsExpanded())
                                                   .or(withPluginName(equalTo(UPM_PLUGIN_NAME)))));
        }
        finally
        {
            upm.uninstallPlugin(INSTALLABLE.getKey());
            upm.uninstallPlugin(CANNOT_DISABLE_MODULE.getKey());
        }
    }

    @Test
    public void upgradeSuccessfulMessageAppearsUponUpgrade() throws Exception
    {
        UpgradeablePluginDetails pluginDetails = tab.getPlugin(INSTALLABLE.getKey()).openPluginDetails();
        assertThat(pluginDetails.getMessage(), isEmptyOrNullString());

        try
        {
            pluginDetails.upgrade();
            assertThat(pluginDetails.getMessage(), not(isEmptyOrNullString()));
        }
        finally
        {
            upm.uninstallPlugin(INSTALLABLE.getKey());
        }
    }

    @Test
    public void verifyNumberOfUpgradesAppearsInTabTitle()
    {
        assertThat(upm.getTabLabel(PluginManager.Tab.UPGRADE_TAB), is(equalTo("Upgrade (6)")));
    }

    @Test
    public void verifyNumberOfUpgradesAppearsInTabTitleAfterUpgrade() throws Exception
    {
        UpgradeablePluginDetails pluginDetails = tab.getPlugin(INSTALLABLE.getKey()).openPluginDetails();
        try
        {
            pluginDetails.upgrade();
            assertThat(upm.getTabLabel(PluginManager.Tab.UPGRADE_TAB), is(equalTo("Upgrade (5)")));
        }
        finally
        {
            upm.uninstallPlugin(INSTALLABLE.getKey());
        }
    }

    @Test
    @TestGroups(excludes = {CONFLUENCE}, reason = "Confluence allows v1 plugins to be installed")
    public void verifyNumberOfUpgradesAppearsInTabTitleAfterUpgradeAll() throws Exception
    {
        try
        {
            tab.upgradeAll();
            //after 2 upgrade successes, there were 3 failures that remain on the upgrade tab
            assertThat(upm.getTabLabel(PluginManager.Tab.UPGRADE_TAB), is(equalTo("Upgrade (3)")));
        }
        finally
        {
            upm.uninstallPlugin(INSTALLABLE.getKey());
            upm.uninstallPlugin(CANNOT_DISABLE_MODULE.getKey());
        }
    }

    @Test
    @TestGroups(CONFLUENCE)
    public void verifyNumberOfUpgradesAppearsInTabTitleAfterUpgradeAllInConfluence() throws IOException
    {
        try
        {
            tab.upgradeAll();
            //after 2 upgrade successes, there were 3 failures that remain on the upgrade tab
            assertThat(upm.getTabLabel(PluginManager.Tab.UPGRADE_TAB), is(equalTo("Upgrade (2)")));
        }
        finally
        {
            upm.uninstallPlugin(INSTALLABLE.getKey());
            upm.uninstallPlugin(CANNOT_DISABLE_MODULE.getKey());
            upm.uninstallPlugin(LEGACY_PLUGIN.getKey());
        }
    }
    
    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInUserInstalledSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.expandAllResults(DEFAULT);
    }
    
    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInUserInstalledSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.expandAllResults(DEFAULT);
        tab.collapseAllResults(DEFAULT);
    }

    private static Matcher<? super UpgradeablePlugin> hasPluginDetailsExpanded()
    {
        return new TypeSafeDiagnosingMatcher<UpgradeablePlugin>()
        {
            @Override
            protected boolean matchesSafely(UpgradeablePlugin item, Description mismatchDescription)
            {
                mismatchDescription.appendText((item.isExpanded() ? "was" : "was not") + " expanded");
                return item.isExpanded();
            }

            public void describeTo(Description description)
            {
                description.appendText("an expanded upm-plugin");
            }
        };
    }

    private static Matcher<? super UpgradeablePlugin> withPluginName(final Matcher<? super String> matcher)
    {
        return new TypeSafeDiagnosingMatcher<UpgradeablePlugin>()
        {
            @Override
            protected boolean matchesSafely(UpgradeablePlugin item, Description mismatchDescription)
            {
                mismatchDescription.appendText("plugin name ");
                return matcher.matches(item.getName());
            }

            public void describeTo(Description description)
            {
                description.appendText("plugin name ");
                matcher.describeTo(description);
            }
        };
    }

    private static void reloadPage()
    {
        upm = product.visit(PluginManager.class);
    }

}
