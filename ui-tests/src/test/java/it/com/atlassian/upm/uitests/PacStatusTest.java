package it.com.atlassian.upm.uitests;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.*;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.google.inject.Inject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.*;
import org.junit.runner.RunWith;

import java.io.*;
import java.net.URI;

import static com.atlassian.upm.test.TestPlugins.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class PacStatusTest
{
    private static @Inject TestedProduct<WebDriverTester> product;

    private static PluginManager upm;

    private final static String DISABLED_JSON_KEY = "disabled";

    // Note: this plugin calls the REST resource which is provided by a test plugin
    private static String pacModeRestResource;
    private static boolean pacOriginalStatus;

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);

        pacModeRestResource = getBaseUri().toASCIIString() + "/rest/plugins/1.0/pac-mode";
        pacOriginalStatus = getValue();
    }

    @After
    public void restorePacProperties()
    {
        // Restore the disabled and baseUrl PAC properties
        putValue(pacOriginalStatus);
    }

    @AfterClass
    public static void afterClass()
    {
        // Log out
        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Test
    public void testPacDisabled()
    {
        disablePac();
        upm = product.visit(PluginManager.class);
        assertTrue(upm.isPacDisabled());
    }

    @Test
    public void testPacReachable()
    {
        enablePac();
        upm = product.visit(PluginManager.class);
        assertTrue(upm.isPacReachable());

        withPacBaseUri(URI.create("http://does.not.compute"), new Runnable()
        {
            public void run()
            {
                upm = product.visit(PluginManager.class);
                assertFalse(upm.isPacReachable());
            }
        });


    }

    private void disablePac()
    {
        putValue(true);
    }

    private void enablePac()
    {
        putValue(false);
    }

    /**
     * Performs a GET request to PacModeResource
     */
    private static boolean getValue()
    {
        HttpGet get = new HttpGet();
        get.setURI(URI.create(pacModeRestResource).normalize());
        get.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("admin", "admin"), "UTF-8", false));
        DefaultHttpClient client = new DefaultHttpClient();
        String jsonContent = null;
        try
        {
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() != 200)
            {
                throw new RuntimeException(String.format("Failed to contact the REST endpoint. Reason: %s; Url: %s", response.getStatusLine().getReasonPhrase(), get.getURI()));
            }
            HttpEntity entity = response.getEntity();
            jsonContent = getContent(entity.getContent());
            JSONObject content = new JSONObject(new JSONTokener(jsonContent));
            Boolean disabled = content.getBoolean(DISABLED_JSON_KEY);
            if (disabled == null)
            {
                throw new RuntimeException(String.format("Failed to contact the REST endpoint: null is unexpected in the JSON result. Url: %s, Content: %s", get.getURI(), jsonContent));
            }
            return disabled;
        }
        catch (ClientProtocolException e)
        {
            throw new RuntimeException(String.format("Failed to contact the REST endpoint. Url: %s", get.getURI()), e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(String.format("Failed to contact the REST endpoint. Url: %s", get.getURI()), e);
        }
        catch (JSONException e)
        {
            throw new RuntimeException(String.format("Failed to contact the REST endpoint: the JSON result couldn't be parsed. Url: %s, Content: %s", get.getURI(), jsonContent), e);
        }
    }

    private static String getContent(InputStream stream) throws IOException
    {
        BufferedReader content = new BufferedReader(new InputStreamReader(stream));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = content.readLine()) != null)
        {
            result.append(line);
        }
        return result.toString();
    }


    /**
     * Performs a PUT request to PacModeResource
     */
    private static void putValue(boolean disabled)
    {
        BasicHttpEntity entity = new BasicHttpEntity();

        entity.setContent(new ByteArrayInputStream((String.format("{ \"%s\": %b }", DISABLED_JSON_KEY, Boolean.valueOf(disabled))).getBytes()));
        entity.setContentType("application/vnd.atl.plugins.pac.mode+json");

        HttpPut put = new HttpPut();
        put.setURI(URI.create(pacModeRestResource).normalize());
        put.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("admin", "admin"), "UTF-8", false));
        put.setEntity(entity);

        DefaultHttpClient client = new DefaultHttpClient();
        try
        {
            HttpResponse response = client.execute(put);
            entity.consumeContent();
            if (response.getStatusLine().getStatusCode() != 200)
            {
                String content;
                try
                {
                    content = getContent(response.getEntity().getContent());
                }
                catch (IOException ioe)
                {
                    content = ioe.getMessage();
                }
                throw new RuntimeException(String.format("Failed to contact the REST endpoint. Reason: %s; Url: %s, Content: %s", response.getStatusLine().getReasonPhrase(), put.getURI(), content));
            }
        }
        catch (ClientProtocolException e)
        {
            throw new RuntimeException(String.format("Failed to contact the REST endpoint. Url: %s", put.getURI()), e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(String.format("Failed to contact the REST endpoint. Url: %s", put.getURI()), e);
        }
    }

    private void withPacBaseUri(URI pacUri, Runnable runnable)
    {
        BasicHttpEntity entity = new BasicHttpEntity();
        entity.setContent(new ByteArrayInputStream(("{ \"pac-base-url\": \"" + pacUri.toASCIIString() + "\" }").getBytes()));
        entity.setContentType("application/vnd.atl.plugins.pac.base.url+json");

        HttpPut put = new HttpPut();
        put.setURI(URI.create(getBaseUri().toASCIIString() + "/rest/plugins/1.0/pac-base-url").normalize());
        put.setEntity(entity);
        put.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("admin", "admin"), "UTF-8", false));
        DefaultHttpClient client = new DefaultHttpClient();
        try
        {
            HttpResponse response = client.execute(put);
            response.getEntity().consumeContent();
            if (response.getStatusLine().getStatusCode() != 200)
            {
                throw new RuntimeException("Failed to set PAC base URI");
            }
        }
        catch (ClientProtocolException e)
        {
            throw new RuntimeException("Could not set PAC base URI", e);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Could not set PAC base URI", e);
        }
        try
        {
            runnable.run();
        }
        finally
        {
            HttpDelete delete = new HttpDelete(put.getURI());
            delete.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("admin", "admin"), "UTF-8", false));
            try
            {
                HttpResponse response = client.execute(delete);
                response.getEntity().consumeContent();
                if (response.getStatusLine().getStatusCode() != 200)
                {
                    throw new RuntimeException("Failed to reset PAC base URI");
                }
            }
            catch (ClientProtocolException e)
            {
                throw new RuntimeException("Could reset PAC base URI", e);
            }
            catch (IOException e)
            {
                throw new RuntimeException("Could reset PAC base URI", e);
            }
        }
    }


    private static URI getBaseUri()
    {
        return URI.create(product.getProductInstance().getBaseUrl());
    }
}
