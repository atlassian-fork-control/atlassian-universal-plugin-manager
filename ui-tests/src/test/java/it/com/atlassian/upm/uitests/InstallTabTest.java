package it.com.atlassian.upm.uitests;

import java.io.File;
import java.net.URI;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.AvailablePlugin;
import com.atlassian.upm.pageobjects.AvailablePluginDetails;
import com.atlassian.upm.pageobjects.ChangesRequiringRestart;
import com.atlassian.upm.pageobjects.InstallTab;
import com.atlassian.upm.pageobjects.InstalledPlugin;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.inject.Inject;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.InstallTab.Category.AVAILABLE;
import static com.atlassian.upm.pageobjects.InstallTab.Category.FEATURED;
import static com.atlassian.upm.pageobjects.InstallTab.Category.POPULAR;
import static com.atlassian.upm.pageobjects.InstallTab.Category.SEARCH;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_REQUIRES_RESTART;
import static com.atlassian.upm.test.TestPlugins.JIRA_CALENDAR;
import static com.atlassian.upm.test.TestPlugins.NONDEPLOYABLE;
import static com.atlassian.upm.test.TestPlugins.NOT_ENABLED_BY_DEFAULT;
import static com.atlassian.upm.test.TestPlugins.NO_BINARY_URL;
import static com.atlassian.upm.test.TestPlugins.UNRECOGNISED_MODULE_TYPE;
import static com.atlassian.upm.test.TestPlugins.XML_PLUGIN;
import static com.atlassian.upm.test.UpmTestGroups.FECRU;
import static com.atlassian.upm.test.UpmTestGroups.REFAPP;
import static org.apache.commons.io.FileUtils.toFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class InstallTabTest
{
    private static final String PLUGIN_MISSING_DEPENDENCIES = "This plugin has one or more modules with missing dependencies.";
    private static final String PROBLEM_ACCESSING_PLUGIN = "Problem accessing plugin file at ";
    private static final String CANNOT_ACCESS_PLUGIN = "Cannot access plugin file at ";
    private static final String UNSUPPORTED_PROTOCOL = "The protocol is unsupported.";
    private static final String INVALID_PLUGIN_MESSAGE = "Could not find a handler capable of installing the plugin at ";
    private static final String INSTALLED_BUT_DISABLED = "This plugin was successfully installed. However, it is disabled by default.";
    private static final String INSTALLATION_REQUIRES_RESTART = "Installation of \"Test Plugin v2 (installable, requires restart)\".";
    private static final String TEST_PLUGIN_PATH = "/atlassian-universal-plugin-manager-test-plugin-v2-installable.jar";
    private static final File PLUGIN_FILE = toFile(InstallTabTest.class.getResource(TEST_PLUGIN_PATH));
    private static final String NON_PLUGIN_FILENAME = "README.txt";
    private static final File NON_PLUGIN_FILE = toFile(InstallTabTest.class.getResource("/" + NON_PLUGIN_FILENAME));
    private static final By ANCHOR_TAG = By.tagName("a");

    private static @Inject TestedProduct<WebDriverTester> product;

    private static PluginManager upm;
    private static InstallTab tab;

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
    }

    @AfterClass
    public static void tearDown()
    {
        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Before
    public void loadTab()
    {
        reloadTab();
    }

    @Test
    public void assertThatFeaturedPluginsAreDisplayedWhenSelected() throws Exception
    {
        tab.show(FEATURED);
        assertTrue(tab.isShowing(FEATURED));
    }

    @Test
    public void assertThatPopularPluginsAreDisplayedWhenSelected() throws Exception
    {
        tab.show(POPULAR);
        assertTrue(tab.isShowing(POPULAR));
    }

    @Test
    public void assertThatAvailablePluginsAreDisplayedWhenSelected() throws Exception
    {
        tab.show(AVAILABLE);
        assertTrue(tab.isShowing(AVAILABLE));
    }

    @Test
    public void assertThatClickingUploadDisplaysUploadDialog()
    {
        tab.openUploadPluginDialog();
        assertTrue(tab.isUploadPluginDialogShowing());
    }

    @Test
    public void assertThatClickingAvailablePluginRowShowsPluginDetails() throws Exception
    {
        AvailablePlugin plugin = tab.show(AVAILABLE).getPlugin(INSTALLABLE.getKey());
        plugin.openPluginDetails();
        assertTrue(plugin.isExpanded());
    }

    @Test
    public void assertThatClickingAvailableNotEnabledPluginRowShowsPluginDetails() throws Exception
    {
        AvailablePlugin plugin = tab.show(AVAILABLE).getPlugin(NOT_ENABLED_BY_DEFAULT.getKey());
        plugin.openPluginDetails();
        assertTrue(plugin.isExpanded());
    }

    @Test
    public void assertThatClickingFeaturedPluginRowShowsPluginDetails() throws Exception
    {
        AvailablePlugin plugin = tab.show(FEATURED).getPlugin(NO_BINARY_URL.getKey());
        plugin.openPluginDetails();
        assertTrue(plugin.isExpanded());
    }

    @Test
    public void assertThatClickingPopularPluginRowShowsPluginDetails() throws Exception
    {
        AvailablePlugin plugin = tab.show(POPULAR).getPlugin(INSTALLABLE.getKey());
        plugin.openPluginDetails();
        assertTrue(plugin.isExpanded());
    }

    @Test
    public void assertThatPluginDescriptionLinkWithValidUriIsNotRemoved() throws Exception
    {
        String uri = tab.show(AVAILABLE).
            getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            getDescription().
            findElement(By.cssSelector("a.upm-test-validate-link")).
            getAttribute("href");
        assertThat(uri, is(equalTo("http://localhost:9990/upm/nowhere")));
    }

    @Test
    public void assertThatPluginDescriptionLinkWithMaliciousUriIsRemoved() throws Exception
    {
        String href = tab.show(AVAILABLE).
            getPlugin(NONDEPLOYABLE.getKey()).
            openPluginDetails().
            getDescription().
            findElement(ANCHOR_TAG).
            getAttribute("href");
        assertThat(href, isEmptyOrNullString());
    }

    @Test
    public void assertThatPluginDescriptionLinkOpensInNewWindow() throws Exception
    {
        String target = tab.show(AVAILABLE).
            getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            getDescription().
            findElement(ANCHOR_TAG).
            getAttribute("target");
        assertThat(target, is(equalTo("_blank")));
    }

    @Test
    public void assertThatInstallButtonIsNotDisplayedForPluginsWithNoBinaryUrl() throws Exception
    {
        String className = tab.show(FEATURED).
            getPlugin(NO_BINARY_URL.getKey()).
            openPluginDetails().
            getInstallButton().
            getAttribute("class");
        assertThat(className, containsString("hidden"));
    }

    @Test
    public void assertThatAfterTwoInstallsTheRestartMessageRemainsInstall() throws Exception
    {
        try
        {
            installPlugin(INSTALLABLE_REQUIRES_RESTART);

            // Go to a different tab there should be a message
            reloadTab();

            // Assert that we have the message about restart that we want
            ChangesRequiringRestart changes = upm.getChangesRequiringRestart().showDetails();
            String msgText = changes.getMessageForPlugin(INSTALLABLE_REQUIRES_RESTART.getKey()).getText();
            assertThat(msgText, containsString(INSTALLATION_REQUIRES_RESTART));

            // Install the plugin a second time to make sure we do not have two messages and that they still speak of install, not upgrade
            installPlugin(INSTALLABLE_REQUIRES_RESTART);

            // Go to a different tab and check the messages
            reloadTab();

            // Assert that we have the message about restart that we want
            changes = upm.getChangesRequiringRestart().showDetails();
            msgText = changes.getMessageForPlugin(INSTALLABLE_REQUIRES_RESTART.getKey()).getText();
            assertThat(msgText, containsString(INSTALLATION_REQUIRES_RESTART));
        }
        finally
        {
            upm.getChangesRequiringRestart().cancelChange(INSTALLABLE_REQUIRES_RESTART.getKey());
        }
    }

    @Test
    public void assertThatClickingInstallOnNonEnabledPluginDisplaysAppropriateMessage() throws Exception
    {
        try
        {
            WebElement message = tab.show(AVAILABLE).
                getPlugin(NOT_ENABLED_BY_DEFAULT.getKey()).
                install().
                getMessage();
            assertThat(message.getText(), is(equalTo(INSTALLED_BUT_DISABLED)));
        }
        finally
        {
            uninstallPlugin(NOT_ENABLED_BY_DEFAULT.getKey());
        }
    }

    @Test
    public void assertThatClickingInstallOnNonenabledPluginInstallsNonenabledPlugin() throws Exception
    {
        try
        {
            tab.show(AVAILABLE).
                getPlugin(NOT_ENABLED_BY_DEFAULT.getKey()).
                install();
            InstalledPlugin plugin = upm.openManageExistingTab().
                getPlugin(NOT_ENABLED_BY_DEFAULT.getKey());

            assertTrue(plugin.isDisabled());
        }
        finally
        {
            uninstallPlugin(NOT_ENABLED_BY_DEFAULT.getKey());
        }
    }

    @Test
    public void assertThatClickingInstallCausesPluginToBeInstalled() throws Exception
    {
        try
        {
            WebElement message = tab.show(AVAILABLE).
                getPlugin(NOT_ENABLED_BY_DEFAULT.getKey()).
                install().
                getMessage();
            assertThat(message.getText(), is(equalTo(INSTALLED_BUT_DISABLED)));
        }
        finally
        {
            uninstallPlugin(NOT_ENABLED_BY_DEFAULT.getKey());
        }
    }

    @Test
    public void assertThatPluginCanBeInstalledFromUri() throws Exception
    {
        try
        {
            installPlugin(INSTALLABLE);
            assertTrue(upm.openManageExistingTab().getPlugin(INSTALLABLE.getKey()).isEnabled());
        }
        finally
        {
            uninstallPlugin(INSTALLABLE.getKey());
        }
    }

    @Test
    public void assertThatInstallingPluginWithUnsupportedModuleTypeDisplaysAppropriateMessage() throws Exception
    {
        try
        {
            installPlugin(UNRECOGNISED_MODULE_TYPE);
            WebElement messages = upm.openManageExistingTab().
                getPlugin(UNRECOGNISED_MODULE_TYPE.getKey()).
                openPluginDetails().
                getMessage();

            assertThat(messages.getText(), is(equalTo(PLUGIN_MISSING_DEPENDENCIES)));
        }
        finally
        {
            uninstallPlugin(UNRECOGNISED_MODULE_TYPE.getKey());
        }
    }

    @Test
    public void assertThatPluginInstalledFromUriIsExpanded() throws Exception
    {
        try
        {
            installPlugin(INSTALLABLE);
            InstalledPlugin plugin = upm.openManageExistingTab().
                getPlugin(INSTALLABLE.getKey());

            assertTrue(plugin.isExpanded());
        }
        finally
        {
            uninstallPlugin(INSTALLABLE.getKey());
        }
    }

    @Test
    public void assertThatNonEnabledPluginCanBeInstalledFromUri() throws Exception
    {
        try
        {
            installPlugin(NOT_ENABLED_BY_DEFAULT);
            WebElement message = upm.openManageExistingTab().
                getPlugin(NOT_ENABLED_BY_DEFAULT.getKey()).
                openPluginDetails().
                getMessage();
            assertThat(message.getText(), is(equalTo(INSTALLED_BUT_DISABLED)));
        }
        finally
        {
            uninstallPlugin(NOT_ENABLED_BY_DEFAULT.getKey());
        }
    }

    @Test
    @TestGroups(excludes = {REFAPP, FECRU},
                reason = "Refapp and Fisheye do not have a DynamicPluginLoader that can load an XML Plugin artifact.")
    public void assertThatXmlPluginCanBeInstalledFromUri() throws Exception
    {
        try
        {
            installPlugin(XML_PLUGIN);
            InstalledPlugin plugin = upm.openManageExistingTab().
                getPlugin(XML_PLUGIN.getKey());

            assertTrue(plugin.isEnabled());
        }
        finally
        {
            uninstallPlugin(XML_PLUGIN.getKey());
        }
    }

    @Test
    public void assertThatUploadingLocalFileInstallsPlugin() throws Exception
    {
        try
        {
            tab.uploadPlugin(PLUGIN_FILE);
            assertTrue(upm.openManageExistingTab().getPlugin(INSTALLABLE.getKey()).isEnabled());
        }
        finally
        {
            uninstallPlugin(INSTALLABLE.getKey());
        }
    }

    @Test
    public void assertThatUploadedPluginWithUnknownFileTypeDisplaysErrorWithFileName() throws Exception
    {
        tab.uploadPlugin(NON_PLUGIN_FILE);
        PageElement messages = upm.getMessages();

        assertThat(messages.getText(), containsString(INVALID_PLUGIN_MESSAGE + NON_PLUGIN_FILENAME));
    }

    @Test
    public void assertThatUploadedPluginWithUnknownFileTypeDisplaysErrorWithFileNameEvenAfterThreeAttempts() throws Exception
    {
        // UPM-977 There was a bug such that multiple unknown file type error would not correctly refresh the XSRF token
        // so on the second or third uploads you would see the XSRF error message.
        for (int i = 0; i < 3; i++)
        {
            tab.uploadPlugin(NON_PLUGIN_FILE);
            PageElement messages = upm.getMessages();

            assertThat(messages.getText(), containsString(INVALID_PLUGIN_MESSAGE));
        }
    }

    @Test
    public void assertThatUploadingLocalFileExpandsInstalledPlugin() throws Exception
    {
        try
        {
            tab.uploadPlugin(PLUGIN_FILE);
            InstalledPlugin plugin = upm.openManageExistingTab().
                getPlugin(INSTALLABLE.getKey());

            assertTrue(plugin.isExpanded());
        }
        finally
        {
            uninstallPlugin(INSTALLABLE.getKey());
        }
    }

    @Test
    public void assertThatInstallingPluginFromUriWithUnknownFileTypeDisplaysErrorWithUri() throws Exception
    {
        URI uri = getBaseUri().resolve("/rest/fakepac/1.0/nonplugin");
        tab.uploadPlugin(uri);
        PageElement messages = upm.getMessages();

        assertThat(messages.getText(), containsString(PROBLEM_ACCESSING_PLUGIN + uri));
    }

    @Test
    public void assertThatInstallingPluginFromFileProtocolUriDisplaysUnsupportedProtocolError() throws Exception
    {
        URI uri = URI.create("file:///unf.unf.unf");
        tab.uploadPlugin(uri);
        PageElement messages = upm.getMessages();

        assertThat(messages.getText(), allOf(containsString(CANNOT_ACCESS_PLUGIN + uri),
                                             containsString(UNSUPPORTED_PROTOCOL)));
    }

    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInInstallAllSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.show(AVAILABLE);
        tab.expandAllResults(AVAILABLE);
    }

    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInInstallFeaturedSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.show(FEATURED);
        tab.expandAllResults(FEATURED);
    }

    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInInstallPopularSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.show(POPULAR);
        tab.expandAllResults(POPULAR);
    }

    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInInstallSearchSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.doSearch("test");
        tab.expandAllResults(SEARCH);
    }

    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInInstallAllSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.show(AVAILABLE);
        tab.expandAllResults(AVAILABLE);
        tab.collapseAllResults(AVAILABLE);
    }

    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInInstallFeaturedSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.show(FEATURED);
        tab.expandAllResults(FEATURED);
        tab.collapseAllResults(FEATURED);
    }

    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInInstallPopularSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.show(POPULAR);
        tab.expandAllResults(POPULAR);
        tab.collapseAllResults(POPULAR);
    }

    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInInstallSearchSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.doSearch("test");
        tab.expandAllResults(SEARCH);
        tab.collapseAllResults(SEARCH);
    }

    @Test
    public void assertThatExpandingPluginWithUnavailableDetailsShowsErrorMessage() throws Exception
    {
        // in fake PAC, the JIRA Calendar plugin appears in the Featured list, but trying to get its details will produce a 404 error
        tab.waitUntilTabIsLoaded();
        AvailablePlugin plugin = tab.show(FEATURED).getPlugin(JIRA_CALENDAR.getKey());
        AvailablePluginDetails details = plugin.openPluginDetails();
        assertTrue(details.isErrorMessage());
    }

    private static void reloadTab()
    {
        upm = product.visit(PluginManager.class);
        tab = upm.openInstallTab();
    }

    private static void installPlugin(TestPlugins testPlugin)
    {
        tab.uploadPlugin(testPlugin.getDownloadUri(getBaseUri()));
    }

    private static URI getBaseUri()
    {
        return URI.create(product.getProductInstance().getBaseUrl());
    }

    private static void uninstallPlugin(String key)
    {
        upm.openManageExistingTab().uninstallPlugin(key);
    }
}
