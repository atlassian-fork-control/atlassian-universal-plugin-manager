package it.com.atlassian.upm.uitests;

import java.io.File;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.InstallTab;
import com.atlassian.upm.pageobjects.ManageExistingTab;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.inject.Inject;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.upm.test.TestPlugins.UPM;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class SelfUpgradeTest
{
    private static final String UPM_CURRENT_VERSION_JAR_NAME = "atlassian-universal-plugin-manager-plugin.jar";
    private static final File UPM_CURRENT_VERSION_JAR = new File(SelfUpgradeTest.class.getClassLoader().getResource(UPM_CURRENT_VERSION_JAR_NAME).getFile());
    private static final String UPM_HIGHER_VERSION_JAR_NAME = "atlassian-universal-plugin-manager-plugin-selfupgrade-test.jar";
    private static final File UPM_HIGHER_VERSION_JAR = new File(SelfUpgradeTest.class.getClassLoader().getResource(UPM_HIGHER_VERSION_JAR_NAME).getFile());
    private static final String UPM_HIGHER_VERSION = System.getProperty("upm.test.upgrade.version");
    
    private static @Inject TestedProduct<WebDriverTester> product;
    
    private static PluginManager upm;

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
    }

    @AfterClass
    public static void tearDown()
    {
        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Test
    public void assertThatSelfUpgradeFromFileIsSuccessful() throws Exception
    {
        upm.openInstallTab().uploadSelfUpgradePlugin(UPM_HIGHER_VERSION_JAR);
        
        ManageExistingTab tab = upm.openManageExistingTab();
        assertEquals(UPM_HIGHER_VERSION, tab.getPlugin(UPM.getKey()).openPluginDetails().getVersion());
        
        // Verify that the "please refresh page" banner appears; it'd be better to put this assertion
        // in a separate test, but currently we are limited to doing everything in one test run
        // because we can't revert the upgrade.
        assertTrue(tab.hasRequiresRefreshMessage());
    }

    @Test
    public void assertThatSelfDowngradeIsDisallowed() throws Exception
    {
        upm.openInstallTab().uploadSelfUpgradePlugin(UPM_HIGHER_VERSION_JAR);

        InstallTab installTab = upm.openInstallTab();
        installTab.uploadSelfUpgradePlugin(UPM_CURRENT_VERSION_JAR);

        assertThat(installTab.getErrorMessage().getText(), containsString("Cannot install a lower version"));
    }
}
