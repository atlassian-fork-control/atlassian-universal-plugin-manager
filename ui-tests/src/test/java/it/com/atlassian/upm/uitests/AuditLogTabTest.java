package it.com.atlassian.upm.uitests;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.upm.pageobjects.AuditLogTab;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.test.RestTester;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.common.base.Function;
import com.google.inject.Inject;
import com.sun.syndication.feed.atom.Feed;

import org.apache.commons.lang.time.DateUtils;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.jdom.Element;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.test.TestPlugins.BUNDLED;
import static com.atlassian.upm.test.UpmTestGroups.BAMBOO;
import static com.atlassian.upm.test.UpmTestGroups.CONFLUENCE;
import static com.atlassian.upm.test.UpmTestGroups.FECRU;
import static com.atlassian.upm.test.UpmTestGroups.JIRA;
import static com.atlassian.upm.test.UpmTestGroups.REFAPP;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class AuditLogTabTest
{
    @Inject private static TestedProduct<WebDriverTester> product;
    @Inject private static RestTester restTester = new RestTester();
    ApplicationProperties applicationProperties = ApplicationPropertiesImpl.getStandardApplicationProperties();
    private static PluginManager upm;
    private static AuditLogTab tab;


    private static final String[] PARSE_DATE_FORMAT = {
        "EEE dd MMM yyyy HH:mm:ss a z",
        "MMMMM dd, yyyy H:mm:ss a z",
        "EEE MMM dd HH:mm:ss yyyy",
        ((SimpleDateFormat) DateFormat.getDateTimeInstance()).toPattern(),
        ((SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG)).toPattern(),
        ((SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)).toPattern()
    };

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
    }

    @AfterClass
    public static void tearDown()
    {
        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Before
    public void loadTab() throws URISyntaxException
    {
        restTester.enablePlugin(BUNDLED);
        restTester.purgeAuditLogFeed();
        reloadTab();
    }

    @Test
    public void testAuditLogIsEmptyOnStart()
    {
        assertThat(tab.getTableRowCount(), is(equalTo(0)));
    }

    @Test
    public void testAuditLogUsesAUITables() {
        addLogMessages(1);
        assertThat(tab.getAuditLogTable().getAttribute("class"), is("aui"));
    }

    @Test
    public void testAuditLogIsNotEmptyAfterDisablingPlugin() throws URISyntaxException
    {
        restTester.disablePlugin(BUNDLED);
        reloadTab();
        assertThat(tab.getTableRowCount(), is(equalTo(1)));
    }

    @Test
    public void assertThatNextButtonGoesToNextPage()
    {
        addLogMessages(30);
        tab.goToNextPage();

        assertThat(tab.getAuditLogCountText(), is(equalTo("Showing entries 26 - 30 of 30")));
    }

    @Test
    public void assertThatPreviousButtonGoesToPreviousPage()
    {
        addLogMessages(30);
        tab.goToNextPage();
        tab.goToPreviousPage();

        assertThat(tab.getAuditLogCountText(), is(equalTo("Showing entries 1 - 25 of 30")));
    }
    
    @Test
    public void assertThatLastButtonGoesToLastPage()
    {
        addLogMessages(52);
        tab.goToLastPage();
        
        assertThat(tab.getAuditLogCountText(), is(equalTo("Showing entries 51 - 52 of 52")));
    }
    
    @Test
    public void assertThatFirstButtonGoesToFirstPage()
    {
        addLogMessages(52);
        tab.goToLastPage();
        tab.goToFirstPage();
        
        assertThat(tab.getAuditLogCountText(), is(equalTo("Showing entries 1 - 25 of 52")));
    }
    
    @Test
    public void testAuditLogRefreshesWhenSwitchingTabs() throws URISyntaxException
    {
        // disable plugin to add an audit log entry
        restTester.disablePlugin(BUNDLED);
    
        // initial click in audit log tab loads the page
        reloadTab();
    
        // switch to another tab
        upm.openManageExistingTab();
    
        // re-enable plugin so we have new audit log entry to check
        restTester.enablePlugin(BUNDLED);
    
        // UPM-286 - Make sure that audit log is refreshed when we switch to the tab
        reloadTab();
        assertThat(tab.getTableRowCount(), is(equalTo(2)));
    }
    
    @Test
    public void testAuditLogRefreshesWhenRefreshButtonIsClicked() throws URISyntaxException
    {
        // disable plugin to add an audit log entry
        restTester.disablePlugin(BUNDLED);

        // initial click in audit log tab loads the page with one audit log entry
        reloadTab();

        // re-enable plugin so we have new audit log entry to check
        restTester.enablePlugin(BUNDLED);

        // UPM-231 - Make sure audit log refreshes when refresh button is clicked, and should display four entries
        tab.refreshLog();
        assertThat(tab.getTableRowCount(), is(equalTo(2)));
    }

    @Test
    public void testAuditLogEntriesAreSortedDescendingByDate() throws URISyntaxException
    {
        // enter safe mode to add more entries to the audit log
        restTester.enterSafeMode();
        // exit safe mode and add few more entries to the audit log
        restTester.exitSafeModeAndRestoreSavedConfiguration();
        reloadTab();
        assertThat(getAuditLogDates(), is(descendingOrder));
        
    }
    
    @Test
    public void testConfigurePurgePolicyLinkOpensPurgePolicyForm()
    {
        tab.openPurgePolicy();
        assertTrue(tab.isPurgePolicyFormOpen());
    }
    
    @Test
    public void testAuditLogPurgeInputDoesNotAllowNonNumericsAndDisplaysProperError() throws URISyntaxException, IOException
    {
        final String numberOfDays = "90";
        tab.enterPurgePolicy(numberOfDays);
        tab.enterPurgePolicy("blah");
        
        assertThat(tab.getMessageText(), is(startsWith("The purge policy must be a positive number")));
    }
    
    @Test
    public void testAuditLogPurgeInputDoesNotAllowNegativeNumericsAndDisplaysProperError() throws URISyntaxException, IOException
    {
        final String numberOfDays = "-1";
        tab.enterPurgePolicy(numberOfDays);
        tab.enterPurgePolicy("blah");
        
        assertThat(tab.getMessageText(), is(startsWith("The purge policy must be a positive number")));
    }
    
    @Test
    public void testAuditLogPurgeInputDoesNotAllowTooLargeNumbersAndDisplaysProperError() throws URISyntaxException, IOException
    {
        final String numberOfDays = "90000";
        tab.enterPurgePolicy(numberOfDays);
        tab.enterPurgePolicy("blah");
        
        assertThat(tab.getMessageText(), is(startsWith("The purge policy must be a positive number less than 100000")));
    }
    
    @Test
    public void testAuditLogPurgeInputDoesNotAllowNonNumericsAndRetainsOldPolicy() throws URISyntaxException, IOException
    {
        final String numberOfDays = "90";
        tab.enterPurgePolicy(numberOfDays);
        tab.enterPurgePolicy("blah");
        
        assertThat(tab.getCurrentPurgePolicyDays(), is(equalTo("90")));
    }
    
    @Test
    public void testPurgePolicyTextIsUpdatedWhenPurgePolicyChanges() throws URISyntaxException, IOException
    {
        final String numberOfDays = "99";
        tab.enterPurgePolicy(numberOfDays);
        
        assertThat(tab.getPurgePolicyText(), is(equalTo("Changes to the plugin system in the last 99 days.")));
    }
    
    @Test
    public void testPurgePolicyTextIsSingularWhenPurgePolicyIsOneDay() throws URISyntaxException, IOException
    {
        final String numberOfDays = "1";
        tab.enterPurgePolicy(numberOfDays);
        
        assertThat(tab.getPurgePolicyText(), is(equalTo("Changes to the plugin system in the last day.")));
    }
    
    @Test
    @TestGroups(CONFLUENCE)
    public void assertThatProfileLinkGoesToConfluenceProfilePage() throws Exception
    {
        assertThatProfileLinkGoesToApplicationProfilePage(applicationProperties.getBaseUrl() + "/display/~admin");
    }

    @Test
    @TestGroups(JIRA)
    public void assertThatProfileLinkGoesToJiraProfilePage() throws Exception
    {
        assertThatProfileLinkGoesToApplicationProfilePage(applicationProperties.getBaseUrl() + "/secure/ViewProfile.jspa?name=admin");
    }

    @Test
    @TestGroups(FECRU)
    @Ignore("UPM-731 The audit log doesn't work right in Fecru yet.")
    public void assertThatProfileLinkGoesToFeCruProfilePage() throws Exception
    {
        assertThatProfileLinkGoesToApplicationProfilePage(applicationProperties.getBaseUrl() + "/user/admin");
    }

    @Test
    @TestGroups(BAMBOO)
    public void assertThatProfileLinkGoesToBambooProfilePage() throws Exception
    {
        assertThatProfileLinkGoesToApplicationProfilePage(applicationProperties.getBaseUrl() + "/browse/user/admin");
    }

    @Test
    @TestGroups(REFAPP)
    public void assertThatProfileLinkGoesToNoWhereInRefapp() throws Exception
    {
        assertThatProfileLinkGoesToApplicationProfilePage(applicationProperties.getBaseUrl() + "/plugins/servlet/users/admin");
    }
    
    /**
     * This test only runs in refapp because the test resources for other applications don't have any other admin users
     * configured automatically.
     */
    @Test
    @TestGroups(REFAPP)
    public void testAuditLogEntriesShowCorrectAuthors() throws URISyntaxException
    {
        RestTester restTesterFred = new RestTester("fred", "fred");

        try
        {
            restTester.disablePlugin(BUNDLED);
            restTesterFred.enablePlugin(BUNDLED);

            reloadTab();

            assertThat(tab.getAuthors(), contains("Fred Sysadmin", "A. D. Ministrator (Sysadmin)"));
        }
        finally
        {
            restTesterFred.destroy();
        }
    }
    
    private Iterable<Date> getAuditLogDates()
    {
        return transform(tab.getAuditLogEntryDates(), new Function<WebElement, Date>()
        {
            public Date apply(final WebElement from)
            {
                try
                {
                    return DateUtils.parseDate(from.getText(), PARSE_DATE_FORMAT);
                }
                catch (ParseException e)
                {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private static void reloadTab()
    {
        upm = product.visit(PluginManager.class);
        tab = upm.openAuditLogTab();
    }
    
    public void addLogMessages(int messageCount)
    {
        int beforeCount = getLogCountFromFeed(restTester.getAuditLogFeed());
        restTester.fillAuditLogEntries(Collections.nCopies(messageCount, "Dummy entry"));
        int afterCount = getLogCountFromFeed(restTester.getAuditLogFeed());
        int added = afterCount - beforeCount;
        checkState(messageCount == added, "Expected to add " + messageCount + " messages to the audit log but instead added " + added);
        reloadTab();
    }

    private int getLogCountFromFeed(Feed feed)
    {
        @SuppressWarnings("unchecked")
        ArrayList<Element> elementList = ((ArrayList<Element>) feed.getForeignMarkup());
        for (Element element : elementList)
        {
            if (element.getName().equals("totalEntries"))
            {
                return Integer.parseInt(element.getContent(0).getValue());
            }
        }
        return -1;
    }

    private static final TypeSafeDiagnosingMatcher<Iterable<Date>> descendingOrder = new TypeSafeDiagnosingMatcher<Iterable<Date>>()
    {
        @Override
        protected boolean matchesSafely(Iterable<Date> dates, Description mismatchDescription)
        {
            Date prev = null;
            for (Date d : dates)
            {
                if (prev == null)
                {
                    prev = d;
                }
                else
                {
                    if (prev.compareTo(d) < 0)
                    {
                        mismatchDescription.appendText("was ").appendValueList("[", ", ", "]", dates);
                        return false;
                    }
                    prev = d;
                }
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("dates sorted in descending order");
        }
    };
    


    private void assertThatProfileLinkGoesToApplicationProfilePage(String profileLink) throws URISyntaxException, IOException
    {
        restTester.disablePlugin(BUNDLED);
        reloadTab();
        assertThat(tab.getFirstAuditLogProfileLink(), is(equalTo(profileLink)));
    }
}
