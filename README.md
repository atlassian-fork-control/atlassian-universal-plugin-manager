# Atlassian Universal Plugin Manager

This repository contains the final version (1.6.4) of the [Atlassian Universal Plugin Manager][1] (UPM) to be released as open source under a [modified BSD license][2]. Versions 2.0 and later are closed-source projects.

For internal Atlassian developers looking for the latest source code, please refer to [this repository](https://bitbucket.org/atlassian-marketplace/atlassian-upm).

# Why release this code?

We've referred to UPM in several contexts for examples of state-of-the-art plugin design, including:

* RESTful web resource design
* Building a dynamic UI with JavaScript
* Advanced OSGi use cases
* Cross-product support
* Functional testing

We believe that these examples are still of value and deserve to be more widely emulated.

# May I use this code in my own plugin?

Yes. This version of UPM is licensed under the [BSD license][2], which means you are free to copy, modify, or ship any part of it for your own needs as long as you follow the terms of the license. Feel free to fork this repository or to clone it locally for your own needs. 

# Where can I get support for this code?

This code is not supported by Atlassian or any of its partners or clients. No issues can be filed here, and pull requests will be ignored. If you have issues with the bundled UPM plugin in your Atlassian product, please visit [support.atlassian.com][3].

If you wish to discuss this code with other developers, we encourage you to visit [answers.atlassian.com][4]. 

[1]: http://confluence.atlassian.com/display/UPM/Universal+Plugin+Manager+Documentation
[2]: http://www.opensource.org/licenses/BSD-3-Clause
[3]: http://support.atlassian.com
[4]: http://answers.atlassian.com