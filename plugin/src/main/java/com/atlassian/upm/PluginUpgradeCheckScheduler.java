package com.atlassian.upm;

import java.util.Date;
import java.util.Map;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.upm.impl.NamespacedPluginSettings;
import com.atlassian.upm.pac.PacClient;

import com.google.common.collect.ImmutableMap;

import org.slf4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.slf4j.LoggerFactory.getLogger;

public class PluginUpgradeCheckScheduler implements LifecycleAware
{
    public static final String KEY_PREFIX = "com.atlassian.upm";
    public static final String KEY = ":plugin-upgrades:";

    private static final long ONE_DAY = 24 * 60 * 60 * 1000;
    private static final Logger log = getLogger(PluginUpgradeCheckScheduler.class);

    private final PluginScheduler pluginScheduler;
    private final PacClient pacClient;
    private final PluginSettingsFactory pluginSettingsFactory;

    public PluginUpgradeCheckScheduler(PluginScheduler pluginScheduler,
        PacClient pacClient,
        PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginScheduler = checkNotNull(pluginScheduler, "pluginScheduler");
        this.pacClient = checkNotNull(pacClient, "pacClient");
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory, "pluginSettingsFactory");
    }

    public void onStart()
    {
        pluginScheduler.scheduleJob(
            "upmPluginUpgradeCheckJob",
            PluginUpgradeCheckJob.class,
            ImmutableMap.<String, Object> of(
                "pacClient", pacClient,
                "pluginSettingsFactory", pluginSettingsFactory),
            new Date(),
            ONE_DAY);
    }

    public static final class PluginUpgradeCheckJob implements PluginJob
    {
        public void execute(final Map<String, Object> jobDataMap)
        {
            PacClient pacClient = (PacClient) jobDataMap.get("pacClient");
            PluginSettingsFactory pluginSettingsFactory = (PluginSettingsFactory) jobDataMap.get("pluginSettingsFactory");
            try
            {
                int upgradeCount = pacClient.getUpgradeCount();
                PluginSettings pluginSettings =
                    new NamespacedPluginSettings(pluginSettingsFactory.createGlobalSettings(), KEY_PREFIX);
                pluginSettings.put(KEY, String.valueOf(upgradeCount));
            }
            catch (Exception e)
            {
                log.warn("Automatic plugin upgrade check failed", e);
            }
        }
    }

}
