package com.atlassian.upm;

import com.atlassian.upm.spi.Plugin;

import com.google.common.collect.ImmutableList;

/**
 * Contains statuses of specified plugins with respect to a particular product version upgrade
 */
public final class ProductUpgradePluginCompatibility
{
    private final Iterable<Plugin> compatible;
    private final Iterable<Plugin> upgradeRequired;
    private final Iterable<Plugin> upgradeRequiredAfterProductUpgrade;
    private final Iterable<Plugin> incompatible;
    private final Iterable<Plugin> unknown;

    private ProductUpgradePluginCompatibility(Builder builder)
    {
        compatible = builder.compatible.build();
        upgradeRequired = builder.upgradeRequired.build();
        upgradeRequiredAfterProductUpgrade = builder.upgradeRequiredAfterProductUpgrade.build();
        incompatible = builder.incompatible.build();
        unknown = builder.unknown.build();
    }

    /**
     * Get plugin info of the plugins that are compatible with the product upgrade.
     *
     * @return the plugin info of the plugins that are compatible with the product upgrade
     */
    public Iterable<Plugin> getCompatible()
    {
        return compatible;
    }

    /**
     * Get the plugin info of the plugins that must be upgraded to work with the product upgrade.
     *
     * @return the plugin info of the plugins that must be upgraded to work with the product upgrade
     */
    public Iterable<Plugin> getUpgradeRequired()
    {
        return upgradeRequired;
    }

    /**
     * Get the plugin info of the plugins that must be upgraded to work with the product upgrade, but the upgraded version is not compatible with the current product version.
     *
     * @return the plugin info of the plugins that must be upgraded to work with the product upgrade, but the upgraded version is not compatible with the current product version
     */
    public Iterable<Plugin> getUpgradeRequiredAfterProductUpgrade()
    {
        return upgradeRequiredAfterProductUpgrade;
    }

    /**
     * Get the plugin info of the plugins that are incompatible with the product upgrade.
     *
     * @return the plugin info of the plugins that are incompatible with the product upgrade
     */
    public Iterable<Plugin> getIncompatible()
    {
        return incompatible;
    }

    /**
     * Get the plugin info of the plugins that are unknown to the plugin data server.
     *
     * @return the plugin info of the plugins that are unknown to the plugin data server
     */
    public Iterable<Plugin> getUnknown()
    {
        return unknown;
    }

    public static class Builder
    {
        private ImmutableList.Builder<Plugin> compatible;
        private ImmutableList.Builder<Plugin> upgradeRequired;
        private ImmutableList.Builder<Plugin> upgradeRequiredAfterProductUpgrade;
        private ImmutableList.Builder<Plugin> incompatible;
        private ImmutableList.Builder<Plugin> unknown;


        public Builder()
        {
            compatible = new ImmutableList.Builder<Plugin>();
            upgradeRequired = new ImmutableList.Builder<Plugin>();
            upgradeRequiredAfterProductUpgrade = new ImmutableList.Builder<Plugin>();
            incompatible = new ImmutableList.Builder<Plugin>();
            unknown = new ImmutableList.Builder<Plugin>();
        }

        public Builder addCompatible(Plugin compatiblePlugin)
        {
            this.compatible.add(compatiblePlugin);
            return this;
        }

        public Builder addUpgradeRequired(Plugin upgradeRequiredPlugin)
        {
            this.upgradeRequired.add(upgradeRequiredPlugin);
            return this;
        }

        public Builder addUpgradeRequiredAfterProductUpgrade(Plugin upgradeRequiredAfterProductUpgradePlugin)
        {
            this.upgradeRequiredAfterProductUpgrade.add(upgradeRequiredAfterProductUpgradePlugin);
            return this;
        }

        public Builder addIncompatible(Plugin incompatiblePlugin)
        {
            this.incompatible.add(incompatiblePlugin);
            return this;
        }

        public Builder addUnknown(Plugin unknownPlugin)
        {
            this.unknown.add(unknownPlugin);
            return this;
        }

        public ProductUpgradePluginCompatibility build()
        {
            return new ProductUpgradePluginCompatibility(this);
        }
    }
}
