package com.atlassian.upm;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.upm.spi.Plugin;
import com.atlassian.upm.spi.Plugin.Module;

import com.google.common.base.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

/**
 * Factory to produce {@code Plugin} and {@code Module} objects.
 */
public class PluginFactory
{
    private final I18nResolver i18nResolver;

    public PluginFactory(I18nResolver i18nResolver)
    {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    /**
     * Create a {@code Plugin}
     *
     * @param plugin plugins API version of a Plugin
     * @return UPM version of a Plugin
     */
    public Plugin createPlugin(com.atlassian.plugin.Plugin plugin)
    {
        checkNotNull(plugin, "plugin");
        return new PluginImpl(plugin, i18nResolver, this);
    }

    /**
     * Create multiple {@code Plugin}s
     *
     * @param plugins plugins API version of a Plugin
     * @return UPM version of a Plugin
     */
    public Iterable<Plugin> createPlugins(Iterable<com.atlassian.plugin.Plugin> plugins)
    {
        return transform(plugins, new Function<com.atlassian.plugin.Plugin, Plugin>()
        {
            public Plugin apply(com.atlassian.plugin.Plugin from)
            {
                return createPlugin(from);
            }
        });
    }

    /**
     * Create a {@code Module}
     *
     * @param module plugins API version of a ModuleDescriptor
     * @return UPM version of a Module
     */
    public Module createModule(ModuleDescriptor<?> module)
    {
        return createModule(module, createPlugin(module.getPlugin()));
    }

    /**
     * Create a {@code Module} belonging to the specified plugin
     *
     * @param module plugins API version of a ModuleDescriptor
     * @param plugin the plugin parent of the module
     * @return UPM version of a Module
     */
    public Module createModule(ModuleDescriptor<?> module, Plugin plugin)
    {
        checkNotNull(module, "module");
        checkNotNull(plugin, "plugin");
        return new PluginModuleImpl(module, i18nResolver, plugin);
    }

    /**
     * Create multiple {@code Module}s
     *
     * @param modules plugins API version of a ModuleDescriptor
     * @return UPM version of a Module
     */
    public Iterable<Module> createModules(Iterable<ModuleDescriptor<?>> modules)
    {
        return transform(modules, new Function<ModuleDescriptor<?>, Module>()
        {
            public Module apply(ModuleDescriptor<?> from)
            {
                return createModule(from);
            }
        });
    }
}
