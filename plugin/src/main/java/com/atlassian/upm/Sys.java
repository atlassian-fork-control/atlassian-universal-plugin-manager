package com.atlassian.upm;

import static java.lang.Boolean.getBoolean;

public class Sys
{
    public static boolean isDevModeEnabled()
    {
        return getBoolean("atlassian.dev.mode") || getBoolean("jira.dev.mode");
    }

    public static boolean isUpmDebugModeEnabled()
    {
        return getBoolean("atlassian.upm.debug");
    }
}
