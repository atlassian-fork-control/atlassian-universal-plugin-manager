package com.atlassian.upm;

public class Either<LEFT, RIGHT>
{
    private final LEFT left;
    private final RIGHT right;

    private Either(LEFT left, RIGHT right)
    {
        this.left = left;
        this.right = right;
    }

    public static <L, R> Either<L, R> left(L left)
    {
        return new Either<L, R>(left, null);
    }

    public static <L, R> Either<L, R> right(R right)
    {
        return new Either<L, R>(null, right);
    }

    public LEFT left()
    {
        return left;
    }

    public RIGHT right()
    {
        return right;
    }

    public boolean isLeft()
    {
        return left != null;
    }
}