package com.atlassian.upm;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.user.UserResolutionException;

/**
 * Decorated version of {@link com.atlassian.sal.api.user.UserManager} to
 * correct any issues caused by accessing via multiple threads.
 */
public class AsyncTaskAwareUserManager implements UserManager
{
    private final UserManager userManager;

    private ThreadLocal<String> username;

    public AsyncTaskAwareUserManager(final UserManager userManager)
    {
        this.userManager = userManager;
        username = new ThreadLocal<String>();
    }

    public void setRemoteUsername(final String username)
    {
        this.username.set(username);
    }
    
    public void resetRemoteUsername()
    {
        this.username.remove();
    }

    public String getRemoteUsername()
    {
        if (username.get() != null)
        {
            return username.get();
        }
        else
        {
            return userManager.getRemoteUsername();
        }
    }

    public String getRemoteUsername(final HttpServletRequest httpServletRequest)
    {
        String name = userManager.getRemoteUsername(httpServletRequest);
        if (name == null)
        {
            name = getRemoteUsername();
        }
        return name;
    }

    public UserProfile getUserProfile(String username)
    {
        return userManager.getUserProfile(username);
    }

    public boolean isUserInGroup(final String username, final String group)
    {
        return userManager.isUserInGroup(username, group);
    }

    public boolean isAdmin(final String username)
    {
        return userManager.isSystemAdmin(username) || userManager.isAdmin(username);
    }

    public boolean isSystemAdmin(final String username)
    {
        return userManager.isSystemAdmin(username);
    }

    public boolean authenticate(final String username, final String password)
    {
        return userManager.authenticate(username, password);
    }

    public Principal resolve(final String username) throws UserResolutionException
    {
        return userManager.resolve(username);
    }
}
