package com.atlassian.upm.rest.representations;

import java.net.URI;
import java.util.Map;

import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.spi.Plugin;

import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.upm.spi.Permission.GET_AVAILABLE_PLUGINS;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_INSTALL;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang.StringUtils.trim;

/**
 * A JSON representation of a plugin that is available for install.  The representation will be in the form
 * <p/>
 * <pre>
 * {
 *   links: {
 *     "self": "/available/plugin.key",
 *     "binary": "http://...",
 *     "available": "/available",
 *     "installed": "/"
 *   },
 *   key: "...",
 *   name: "...",
 *   icon: {                    // won't be present if there is no icon
 *     width: 270,
 *     height: 90,
 *     link: "http://..."
 *   },
 *   vendor: {                  // won't be present if there is no vendor name or link
 *     name: "...",
 *     link: "http://..."
 *   },
 *   version: "...",
 *   installedVersion: "...",
 *   license: "...",
 *   summary: "...",
 *   description: "...",
 *   releaseNotesUrl: "...",
 *   deployable: "...",
 *   pluginSystemVersion: "..."
 * }
 * </pre>
 */
public class AvailablePluginRepresentation
{
    @JsonProperty private final Map<String, URI> links;
    @JsonProperty private final String key;
    @JsonProperty private final String name;
    @JsonProperty private final Icon icon;
    @JsonProperty private final Vendor vendor;
    @JsonProperty private final String version;
    @JsonProperty private final String installedVersion;
    @JsonProperty private final String license;
    @JsonProperty private final String summary;
    @JsonProperty private final String description;
    @JsonProperty private final String releaseNotesUrl;
    @JsonProperty private final boolean deployable;
    @JsonProperty private final String pluginSystemVersion;
    @JsonProperty private final String restartState;

    @JsonCreator
    public AvailablePluginRepresentation(@JsonProperty("links") Map<String, URI> links,
        @JsonProperty("key") String key,
        @JsonProperty("name") String name,
        @JsonProperty("icon") Icon icon,
        @JsonProperty("vendor") Vendor vendor,
        @JsonProperty("version") String version,
        @JsonProperty("installedVersion") String installedVersion,
        @JsonProperty("license") String license,
        @JsonProperty("summary") String summary,
        @JsonProperty("description") String description,
        @JsonProperty("releaseNotesUrl") String releaseNotesUrl,
        @JsonProperty("deployable") boolean deployable,
        @JsonProperty("pluginSystemVersion") String pluginSystemVersion,
        @JsonProperty("restartState") String restartState)
    {
        this.links = ImmutableMap.copyOf(links);
        this.key = checkNotNull(key, "key");
        this.name = checkNotNull(name, "name");
        this.icon = icon;
        this.vendor = vendor;
        this.version = checkNotNull(version, "version");
        this.installedVersion = installedVersion;
        this.license = license;
        this.summary = summary;
        this.description = description;
        this.releaseNotesUrl = releaseNotesUrl;
        this.deployable = deployable;
        this.pluginSystemVersion = pluginSystemVersion;
        this.restartState = restartState;
    }

    AvailablePluginRepresentation(PluginVersion plugin, UpmUriBuilder uriBuilder, LinkBuilder linkBuilder, PluginAccessorAndController pluginAccessorAndController)
    {
        // This plugin might be null if it is not installed, but that is fine for the permission check
        final Plugin actualPlugin = pluginAccessorAndController.getPlugin(plugin.getPlugin().getPluginKey());
        LinkBuilder.LinksMapBuilder links =
            linkBuilder.buildLinkForSelf(uriBuilder.buildAvailablePluginUri(plugin.getPlugin().getPluginKey()))
                .putIfPermitted(GET_AVAILABLE_PLUGINS, "available", uriBuilder.buildAvailablePluginCollectionUri())
                .put("installed", uriBuilder.buildInstalledPluginCollectionUri())
                .put("details", uriBuilder.buildPacPluginDetailsUri(plugin));
        if (plugin.getBinaryUrl() != null)
        {
            links.putIfPermitted(MANAGE_PLUGIN_INSTALL, actualPlugin, "binary", URI.create(trim(plugin.getBinaryUrl())));
        }
        this.links = links.build();
        this.key = plugin.getPlugin().getPluginKey();
        this.name = plugin.getPlugin().getName();
        this.icon = newIcon(plugin.getPlugin().getIcon());
        this.vendor = newVendor(plugin.getPlugin().getVendor());
        this.version = plugin.getVersion();
        this.installedVersion = getInstalledVersion(pluginAccessorAndController, this.key);
        this.license = plugin.getLicense().getName();
        this.summary = plugin.getSummary();
        this.description = plugin.getDescription();
        this.releaseNotesUrl = plugin.getReleaseNotesUrl();
        if (pluginAccessorAndController.getUpmPluginKey().equals(plugin.getPlugin().getPluginKey()))
        {
            // UPM-1200: PAC will say UPM is not deployable (since older UPMs can't self-upgrade) but for our purposes it is deployable.
            this.deployable = true;
        }
        else
        {
            this.deployable = plugin.getPlugin().isDeployable();
        }
        this.pluginSystemVersion = plugin.getPluginSystemVersion().name();
        this.restartState = RestartState.toString(actualPlugin == null ? PluginRestartState.NONE : pluginAccessorAndController.getRestartState(actualPlugin));
    }

    private String getInstalledVersion(PluginAccessorAndController pluginAccessorAndController, String pluginKey)
    {
        if (pluginAccessorAndController.isPluginInstalled(pluginKey))
        {
            return pluginAccessorAndController.getPlugin(pluginKey).getPluginInformation().getVersion();
        }
        return null;
    }

    private Vendor newVendor(com.atlassian.plugins.domain.model.vendor.Vendor vendor)
    {
        if (vendor == null || isEmpty(vendor.getName()))
        {
            return null;
        }
        return new Vendor(vendor.getName(), isEmpty(vendor.getUrl()) ? null : URI.create(vendor.getUrl()));
    }

    private Icon newIcon(com.atlassian.plugins.domain.model.plugin.PluginIcon icon)
    {
        if (icon == null || icon.getLocation() == null)
        {
            return null;
        }
        return new Icon(icon.getWidth(), icon.getHeight(), URI.create(icon.getLocation()));
    }

    public static final class Icon
    {
        @JsonProperty private final Integer width;
        @JsonProperty private final Integer height;
        @JsonProperty private final URI link;

        @JsonCreator
        public Icon(@JsonProperty("width") Integer width,
            @JsonProperty("height") Integer height,
            @JsonProperty("link") URI link)
        {
            this.width = width;
            this.height = height;
            this.link = checkNotNull(link, "link");
        }

        public Integer getWidth()
        {
            return width;
        }

        public Integer getHeight()
        {
            return height;
        }

        public URI getLink()
        {
            return link;
        }
    }

    public static final class Vendor
    {
        @JsonProperty private final String name;
        @JsonProperty private final URI link;

        @JsonCreator
        public Vendor(@JsonProperty("name") String name, @JsonProperty("link") URI link)
        {
            this.name = checkNotNull(name, "name");
            this.link = link;
        }

        public String getName()
        {
            return name;
        }

        public URI getLink()
        {
            return link;
        }
    }

    public URI getSelfLink()
    {
        return links.get("self");
    }

    public URI getBinaryLink()
    {
        return links.get("binary");
    }

    public URI getInstalledLink()
    {
        return links.get("installed");
    }

    public URI getAvailableLink()
    {
        return links.get("available");
    }

    public URI getDetailsLink()
    {
        return links.get("details");
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public Icon getIcon()
    {
        return icon;
    }

    public Vendor getVendor()
    {
        return vendor;
    }

    public String getVersion()
    {
        return version;
    }

    public String getInstalledVersion()
    {
        return installedVersion;
    }

    public String getLicense()
    {
        return license;
    }

    public String getSummary()
    {
        return summary;
    }

    public String getDescription()
    {
        return description;
    }

    public String getReleaseNotesUrl()
    {
        return releaseNotesUrl;
    }

    public boolean isDeployable()
    {
        return deployable;
    }

    public String getPluginSystemVersion()
    {
        return pluginSystemVersion;
    }

    public String getRestartState()
    {
        return restartState;
    }
}
