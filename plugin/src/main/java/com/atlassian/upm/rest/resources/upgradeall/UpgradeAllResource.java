package com.atlassian.upm.rest.resources.upgradeall;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.PluginDownloadService;
import com.atlassian.upm.PluginInstaller;
import com.atlassian.upm.log.AuditLogService;
import com.atlassian.upm.pac.PacClient;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.async.AsynchronousTaskManager;
import com.atlassian.upm.rest.representations.RepresentationFactory;
import com.atlassian.upm.rest.resources.permission.PermissionEnforcer;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.atlassian.upm.rest.MediaTypes.TASK_ERROR_JSON;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_INSTALL;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A controller resource that allows all plugins to be upgraded.
 */
@Path("/upgrades/all")
public class UpgradeAllResource
{
    private final AsynchronousTaskManager taskManager;
    private final PacClient pacClient;
    private final PluginInstaller pluginInstaller;
    private final RepresentationFactory representationFactory;
    private final PluginAccessorAndController pluginAccessorAndController;
    private final PluginDownloadService pluginDownloadService;
    private final PermissionEnforcer permissionEnforcer;
    private final AuditLogService auditLogger;
    private final UpmUriBuilder uriBuilder;
    private final UserManager userManager;

    public UpgradeAllResource(PluginAccessorAndController pluginAccessorAndController,
        AsynchronousTaskManager taskManager,
        PacClient pacClient,
        PluginDownloadService pluginDownloadService,
        PluginInstaller pluginInstaller,
        RepresentationFactory representationFactory,
        PermissionEnforcer permissionEnforcer,
        AuditLogService auditLogger,
        UpmUriBuilder uriBuilder,
        @Qualifier("asyncTaskAwareUserManager") UserManager userManager)
    {
        this.userManager = checkNotNull(userManager, "userManager");
        this.uriBuilder = checkNotNull(uriBuilder, "uriBuilder");
        this.permissionEnforcer = checkNotNull(permissionEnforcer, "permissionEnforcer");
        this.pluginAccessorAndController = checkNotNull(pluginAccessorAndController, "pluginAccessorAndController");
        this.taskManager = checkNotNull(taskManager, "taskManager");
        this.pacClient = checkNotNull(pacClient, "pacClient");
        this.pluginDownloadService = checkNotNull(pluginDownloadService, "pluginDownloadService");
        this.pluginInstaller = checkNotNull(pluginInstaller, "pluginInstaller");
        this.representationFactory = checkNotNull(representationFactory, "representationFactory");
        this.auditLogger = checkNotNull(auditLogger, "auditLogger");
    }

    @POST
    public Response upgradeAll()
    {
        permissionEnforcer.enforcePermission(MANAGE_PLUGIN_INSTALL);
        if (pluginAccessorAndController.isSafeMode())
        {
            return Response.status(Status.CONFLICT).entity(representationFactory.createI18nErrorRepresentation("upm.upgrade.error.safe.mode")).type(
                TASK_ERROR_JSON).build();
        }
        return taskManager.executeAsynchronousTask(
            new UpgradeAllTask(pacClient, pluginDownloadService, pluginAccessorAndController, pluginInstaller,
                               auditLogger, uriBuilder, userManager.getRemoteUsername()));
    }
}
