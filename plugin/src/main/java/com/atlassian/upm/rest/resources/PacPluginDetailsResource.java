package com.atlassian.upm.rest.resources;

import com.atlassian.plugins.PacException;
import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.upm.pac.PacClient;
import com.atlassian.upm.rest.UpmUriBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static com.atlassian.upm.rest.MediaTypes.PAC_DETAILS_LINK;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;


/**
 * This resource will provide the "details" link for the plugin.
 * it has been extracted from PluginRepresentation because it involves a
 * call to PAC
 */
@Path("/pac-details-link/{pluginKey}/{pluginVersion}")
public class PacPluginDetailsResource
{
    private final PacClient client;
    private final UpmUriBuilder upmUriBuilder;

    private static final Logger log = LoggerFactory.getLogger(PacPluginDetailsResource.class);

    public PacPluginDetailsResource(PacClient client, UpmUriBuilder uriBuilder)
    {
        this.client = checkNotNull(client, "client");
        this.upmUriBuilder = checkNotNull(uriBuilder, "uriBuilder");
    }


    /**
     * Returns the 'details' link of the plugin passed in parameter
     * @param key
     * @param version
     * @return Response representing the link, or representing null if the plugin wasn't found, or
     * a serverError() if PAC returned an exception
     */
    @GET
    @Produces(PAC_DETAILS_LINK)
    public Response get(@PathParam("pluginKey") String key, @PathParam("pluginVersion") String version)
    {
        try
        {
            PluginVersion pluginVersion = client.getPluginVersion(key, version);
            String link = null;
            if (pluginVersion != null)
            {
                link = upmUriBuilder.buildPacPluginDetailsUri(pluginVersion).toString();
                return Response.ok(new PacDetailLinkRepresentation(link)).build();
            }
            return Response.status(NOT_FOUND).build();
        }
        catch (PacException e)
        {
            log.warn(String.format("Could not get the plugin version from PAC (%s, %s): %s", key, version, e.toString()));
            return Response.serverError().build();
        }
    }

    public static final class PacDetailLinkRepresentation
    {
        @JsonProperty
        private String link;

        @JsonCreator
        public PacDetailLinkRepresentation(@JsonProperty("link") String link)
        {
            this.link = link;
        }

        public String getLink()
        {
            return link;
        }
    }
}
