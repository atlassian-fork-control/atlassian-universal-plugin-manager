package com.atlassian.upm.rest.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.plugin.PluginRestartState;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.SafeModeException;
import com.atlassian.upm.rest.representations.PluginRepresentation;
import com.atlassian.upm.rest.representations.RepresentationFactory;
import com.atlassian.upm.rest.resources.permission.PermissionEnforcer;
import com.atlassian.upm.spi.Plugin;

import static com.atlassian.upm.rest.MediaTypes.ERROR_JSON;
import static com.atlassian.upm.rest.MediaTypes.INSTALLED_PLUGIN_JSON;
import static com.atlassian.upm.rest.UpmUriEscaper.unescape;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_ENABLEMENT;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_UNINSTALL;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Provides REST resources for working with plugins
 */
@Path("/{pluginKey}")
public class PluginResource
{
    private final RepresentationFactory representationFactory;
    private final PluginAccessorAndController pluginAccessorAndController;
    private final PermissionEnforcer permissionEnforcer;

    public PluginResource(RepresentationFactory representationFactory, PluginAccessorAndController pluginAccessorAndController,
        PermissionEnforcer permissionEnforcer)
    {
        this.permissionEnforcer = checkNotNull(permissionEnforcer, "permissionEnforcer");
        this.representationFactory = checkNotNull(representationFactory, "representationFactory");
        this.pluginAccessorAndController = checkNotNull(pluginAccessorAndController, "pluginAccessorAndController");
    }

    /**
     * Retrieves a JSON representation of the plugin specified by the {@code pluginKey}.
     * Anyone who has access to UPM has permission to access this resource method.
     *
     * @param pluginKey key of the plugin whose representation will be retrieved
     * @return a {@code Response} for the client with details on the request's success or failure
     */
    @GET
    @Produces(INSTALLED_PLUGIN_JSON)
    public Response get(@PathParam("pluginKey") String pluginKey)
    {
        permissionEnforcer.enforceAdmin();
        pluginKey = unescape(pluginKey);
        Plugin plugin = pluginAccessorAndController.getPlugin(pluginKey);
        if (plugin != null)
        {
            return Response.ok(representationFactory.createPluginRepresentation(plugin)).build();
        }
        return Response.status(NOT_FOUND).build();
    }

    /**
     * Updates a plugin with the given representation.
     *
     * @param pluginRepresentation representation of the plugin to update
     * @param pluginKey key of the plugin to update
     * @return a {@code Response} for the client with details on the request's success or failure
     */
    @PUT
    @Consumes(INSTALLED_PLUGIN_JSON)
    public Response put(@PathParam("pluginKey") String pluginKey, PluginRepresentation pluginRepresentation)
    {
        pluginKey = unescape(pluginKey);
        Plugin plugin = pluginAccessorAndController.getPlugin(pluginKey);
        permissionEnforcer.enforcePermission(MANAGE_PLUGIN_ENABLEMENT, plugin);

        if (plugin == null)
        {
            return Response.status(NOT_FOUND).build();
        }

        if (pluginAccessorAndController.isUpmPlugin(plugin))
        {
            return Response.status(CONFLICT)
                .entity(representationFactory.createI18nErrorRepresentation(
                    "upm.plugin.error.invalid.upm.plugin.action"))
                .type(ERROR_JSON)
                .build();
        }

        // check if enablement status needs to be toggled, and if so do it
        if (pluginAccessorAndController.isPluginEnabled(pluginKey) != pluginRepresentation.isEnabled())
        {
            if (pluginRepresentation.isEnabled())
            {
                if (!pluginAccessorAndController.enablePlugin(pluginKey))
                {
                    return Response.status(INTERNAL_SERVER_ERROR)
                        .entity(representationFactory.createI18nErrorRepresentation(
                            "upm.plugin.error.failed.to.enable"))
                        .type(ERROR_JSON)
                        .build();
                }
            }
            else
            {
                if (!pluginAccessorAndController.disablePlugin(pluginKey))
                {
                    return Response.status(INTERNAL_SERVER_ERROR)
                        .entity(representationFactory.createI18nErrorRepresentation(
                            "upm.plugin.error.failed.to.disable"
                        )).type(ERROR_JSON).build();
                }
            }
        }

        return Response.ok(representationFactory.createPluginRepresentation(plugin)).type(INSTALLED_PLUGIN_JSON).build();
    }

    /**
     * Uninstalls a plugin with the given {@code pluginKey}. System and bundled plugins are not uninstallable, and
     * a {@code FORBIDDEN} response will be returned if user tries to uninstall system and bundled plugins. An
     * {@code OK} response with the {@code PluginRepresentation} entity will be returned on a successful plugin
     * uninstall.
     *
     * @param pluginKey key of the plugin to uninstall
     * @return a {@code Response} for the client with details on the request's success or failure
     */
    @DELETE
    public Response uninstallPlugin(@PathParam("pluginKey") String pluginKey)
    {
        pluginKey = unescape(pluginKey);
        Plugin plugin = pluginAccessorAndController.getPlugin(pluginKey);
        permissionEnforcer.enforcePermission(MANAGE_PLUGIN_UNINSTALL, plugin);

        if (plugin == null)
        {
            return Response.status(NOT_FOUND).build();
        }

        if (pluginAccessorAndController.isUpmPlugin(plugin))
        {
            return Response.status(405)
                .entity(representationFactory.createI18nErrorRepresentation(
                    "upm.plugin.error.invalid.upm.plugin.action"))
                .type(ERROR_JSON)
                .build();
        }

        // Check first if plugin can be uninstalled
        checkSystemPlugin(plugin, "upm.pluginUninstall.error.plugin.is.system");
        checkStaticPlugin(plugin, "upm.pluginUninstall.error.plugin.is.static");
        checkUninstallable(plugin, "upm.pluginUninstall.error.plugin.not.uninstallable");

        try
        {
            pluginAccessorAndController.uninstallPlugin(plugin);
        }
        catch (SafeModeException e)
        {
            return Response.status(CONFLICT)
                .entity(representationFactory.createI18nErrorRepresentation("upm.pluginUninstall.error.safe.mode"))
                .type(ERROR_JSON)
                .build();
        }
        if (pluginAccessorAndController.getPlugin(pluginKey) == null)
        {
            // if the plugin was completely removed, return OK
            return Response.ok(representationFactory.createPluginRepresentation(plugin)).type(INSTALLED_PLUGIN_JSON).build();
        }
        else if (PluginRestartState.REMOVE.equals(pluginAccessorAndController.getRestartState(plugin)))
        {
            // if the plugin is scheduled for removal on next restart, return Accepted
            return Response.status(Status.ACCEPTED).entity(representationFactory.createPluginRepresentation(plugin)).type(INSTALLED_PLUGIN_JSON).build();
        }
        else
        {
            // otherwise, removing the plugin failed but no exception was thrown
            return Response.status(INTERNAL_SERVER_ERROR)
                .entity(representationFactory.createI18nErrorRepresentation(
                    "upm.pluginUninstall.error.failed.to.uninstall"
                )).type(ERROR_JSON).build();
        }
    }

    private void checkSystemPlugin(Plugin plugin, String i18nErrorKey)
    {
        checkPluginActionAllowed(!pluginAccessorAndController.isUserInstalled(plugin), i18nErrorKey);
    }

    private void checkStaticPlugin(Plugin plugin, String i18nErrorKey)
    {
        checkPluginActionAllowed(plugin.isStaticPlugin(), i18nErrorKey);
    }

    private void checkUninstallable(Plugin plugin, String i18nErrorKey)
    {
        checkPluginActionAllowed(!plugin.isUninstallable(), i18nErrorKey);
    }

    private void checkPluginActionAllowed(boolean isNotAllowed, String i18nErrorKey)
    {
        if (isNotAllowed)
        {
            throw new WebApplicationException(Response.status(FORBIDDEN)
                .type(ERROR_JSON)
                .entity(representationFactory.createI18nErrorRepresentation(i18nErrorKey)).build());
        }
    }
}
