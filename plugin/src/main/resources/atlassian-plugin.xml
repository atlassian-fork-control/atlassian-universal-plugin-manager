<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <!-- Add UPM admin menu item to Refapp Administration page. The UPM menu item will be placed under the "General"
         sub-menu of the Administration page. By default, it is placed at the top of the section (noted by the weight) -->
    <web-item key="refimpl-upm-admin-menu" name="Universal Plugin Manager" section="system.admin/general"  weight="1" application="refapp">
        <label key="upm.title"/>
        <link linkId="upm-admin-link">/plugins/servlet/upm</link>
    </web-item>
    
    <!-- Add UPM admin menu item to Confluence Administration page. We don't need application="confluence" here because
         it wouldn't work, and Confluence will just disregard it. The UPM menu item will be placed under the
         "Configuration" sub-menu of the Administration page. By default, it is placed under the "Manage Referrers" menu
         item (noted by the weight) -->
    <web-item key="confluence-upm-admin-menu" name="Universal Plugin Manager" section="system.admin/configuration"  weight="40">
        <label key="upm.title"/>
        <link linkId="upm-admin-link">/plugins/servlet/upm</link>
    </web-item>

    <!-- Add UPM admin menu item to JIRA Administration page. The UPM menu item will be placed under the "System"
         sub-menu of the Administration page. By default, it is placed under the "Oauth" menu item (noted by the weight)
         PRIOR to 4.4 -->
    <web-item key="jira-upm-admin-menu" name="Universal Plugin Manager" section="system.admin/system"  weight="145" application="jira">
        <label key="upm.title"/>
        <link linkId="upm-admin-link">/plugins/servlet/upm</link>
        <condition class="com.atlassian.upm.conditions.IsPriorToJiraVersion">
                <param name="majorVersion">4</param>
                <param name="minorVersion">4</param>
        </condition>
    </web-item>

    <!-- Add UPM admin menu item to JIRA Administration page. The UPM menu item will be placed in its own subsection in the "Plugins" menu
     in the jira admin section. 4.4 and beyond -->
    <web-section key="upm_section" name="Top Plugins Section" location="top_plugins_section" i18n-name-key="webfragments.admin.menu.section.top.plugins.name"  weight="10">
        <label key="upm.title"/>
    </web-section>

    <web-item key="jira-upm-admin-menu-new" name="Universal Plugin Manager" section="top_plugins_section/upm_section"  weight="145" application="jira">
        <label key="upm.title"/>
        <link linkId="upm-admin-link">/plugins/servlet/upm</link>
    </web-item>

    <!-- Add UPM admin menu item to Bamboo Administration page. The UPM menu item will be placed under the "Plugins"
         sub-menu of the Administration page. By default, it is placed under the "Application Links" menu item (noted by the weight) -->
    <web-item key="bamboo-upm-admin-menu" name="Universal Plugin Manager" section="system.admin/plugins"  weight="15" application="bamboo">
        <label key="Plugin Manager"/>
        <link linkId="upm-admin-link">/plugins/servlet/upm</link>
    </web-item>

    <!-- Add UPM admin menu item to Fisheye/Crucible Administration page. The UPM menu item will be placed under the "System"
         sub-menu of the Administration page. By default, it is placed under the "Shutdown" menu item (noted by the weight) -->
    <web-item key="fecru-upm-admin-menu" name="Universal Plugin Manager" section="system.admin/system"  weight="15" application="fisheye">
        <label>Plugins</label>
        <link linkId="upm-admin-link">/plugins/servlet/upm</link>
    </web-item>

    <web-resource name="UPM Web Resources" key="plugin-manager">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <resource type="download" name="upm.css" location="css/upm.css"/>
        <resource type="download" name="ie/upm-ie.css" location="css/ie/upm-ie.css">
            <param name="ieonly" value="true"/>
        </resource>
        <resource type="download" name="js/upm.js" location="js/upm.js"/>
        <resource type="download" name="js/upm.json.js" location="js/upm.json.js"/>
        <resource type="download" name="js/html-sanitizer.js" location="js/html-sanitizer.js"/>
        <resource type="download" name="js/jquery.hashchange.js" location="js/jquery.hashchange.js"/>
    </web-resource>
    <web-resource name="Plugin Upgrade Notification Resources" key="upgrade-notification">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <resource type="download" name="js/upgrade-notification.js" location="js/upgrade-notification.js">
            <param name="cache" value="false" />
        </resource>
        <context>atl.admin</context>
        <transformation extension="js">
            <transformer key="upgradeNotificationTransformer" />
        </transformation>
    </web-resource>
    <web-resource name="Refapp Web Resources" key="refapp">
        <resource type="download" name="refapp.css" location="css/refapp.css"/>
    </web-resource>
    <web-resource name="Fake JSON Data" key="fakedata">
        <resource type="download" name="fakedata/plugindetails.json" location="fakedata/plugindetails.json"/>
        <resource type="download" name="fakedata/pluginlist.json" location="fakedata/pluginlist.json"/>
    </web-resource>

    <web-resource-transformer key="upgradeNotificationTransformer" class="com.atlassian.upm.UpgradeNotificationTransformer" />

    <component-import key="renderer" interface="com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer" />
    <component-import key="pluginSystemEventManager" interface="com.atlassian.plugin.event.PluginEventManager" />
    <component-import key="pluginSettingsFactory" interface="com.atlassian.sal.api.pluginsettings.PluginSettingsFactory" />
    <component-import key="pluginController" interface="com.atlassian.plugin.PluginController" />
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />
    <component-import key="requestFactory" interface="com.atlassian.sal.api.net.RequestFactory" />
    <component-import key="pluginAccessor" interface="com.atlassian.plugin.PluginAccessor" />
    <component-import key="i18nResolver" interface="com.atlassian.sal.api.message.I18nResolver" />
    <component-import key="localeResolver" interface="com.atlassian.sal.api.message.LocaleResolver" />
    <component-import key="txTemplate" interface="com.atlassian.sal.api.transaction.TransactionTemplate" />
    <component-import key="loginUriProvider" interface="com.atlassian.sal.api.auth.LoginUriProvider" />
    <component-import key="pluginMetaDataManager" interface="com.atlassian.plugin.metadata.PluginMetadataManager" />
    <component-import key="salWebSudoManager" interface="com.atlassian.sal.api.websudo.WebSudoManager" />
    <component-import key="salPluginScheduler" interface="com.atlassian.sal.api.scheduling.PluginScheduler" />

    <template-context-item key="userManagerContextItem" component-ref="userManager" context-key="userManager" name="User Manager Context Item"/>
    <template-context-item key="applicationPropertiesContextItem" component-ref="applicationProperties" context-key="applicationProperties" name="Application Properties Context Item"/>
    <template-context-item key="linkBuilderContextItem" component-ref="linkBuilder" context-key="linkBuilder" name="Link Builder Context Item"/>
    <template-context-item key="tabVisibilityContextItem" component-ref="tabVisibility" context-key="tabVisibility" name="Tab Visibility Context Item"/>

    <component key="uriBuilder" class="com.atlassian.upm.rest.UpmUriBuilder" />
    <component key="linkBuilder" class="com.atlassian.upm.rest.representations.LinkBuilder" />
    <component key="tabVisibility" class="com.atlassian.upm.permission.TabVisibilityImpl" />
    <component key="asynchronousTaskManager" class="com.atlassian.upm.rest.async.AsynchronousTaskManager" />
    <component key="representationFactory" class="com.atlassian.upm.rest.representations.RepresentationFactoryImpl" />
    <component key="pluginDownloadService" class="com.atlassian.upm.impl.PluginDownloadServiceImpl" />
    <component key="pluginSettingsConfigurationStore" interface="com.atlassian.upm.ConfigurationStore"
               class="com.atlassian.upm.impl.PluginSettingsConfigurationStore" stateful="true"/>
    <component key="pluginInstaller" class="com.atlassian.upm.PluginInstaller"/>
    <component key="pluginAccessorAndController" class="com.atlassian.upm.PluginAccessorAndControllerImpl"/>
    <component key="auditLogService" interface="com.atlassian.upm.log.AuditLogService"
               class="com.atlassian.upm.log.PluginSettingsAuditLogService" stateful="true"/>
    <component key="pluginLogService" class="com.atlassian.upm.log.PluginLogServiceImpl" public="true">
        <interface>com.atlassian.upm.api.log.PluginLogService</interface>
    </component>
    <component key="pacClient" class="com.atlassian.upm.pac.PacClientImpl" />
    <component key="pacServiceFactory" class="com.atlassian.upm.pac.PacServiceFactoryImpl" />
    <component key="obrPluginInstallHandler" class="com.atlassian.upm.impl.ObrPluginInstallHandler" />
    <component key="asyncTaskAwareUserManager" class="com.atlassian.upm.AsyncTaskAwareUserManager"/>
    <component key="asyncTaskAwareApplicationProperties" class="com.atlassian.upm.AsyncTaskAwareApplicationProperties"/>
    <component key="permissionEnforcer" class="com.atlassian.upm.rest.resources.permission.PermissionEnforcer"/>
    <component key="bundleAccessor" class="com.atlassian.upm.osgi.impl.BundleAccessorImpl"/>
    <component key="serviceAccessor" class="com.atlassian.upm.osgi.impl.ServiceAccessorImpl"/>
    <component key="packageAccessor" class="com.atlassian.upm.osgi.impl.PackageAccessorImpl"/>
    <component key="pluginFactory" class="com.atlassian.upm.PluginFactory"/>
    <component key="tokenManager" class="com.atlassian.upm.token.TokenManagerImpl" />
    <component key="defaultPermissionService" class="com.atlassian.upm.permission.PermissionServiceImpl"/>
    <component key="pluginUpgradeCheckScheduler" interface="com.atlassian.sal.api.lifecycle.LifecycleAware"
               class="com.atlassian.upm.PluginUpgradeCheckScheduler" public="true" />
    <component key="selfUpgradeController" class="com.atlassian.upm.SelfUpgradeController" />
    <component key="selfUpgradePluginAccessor" class="com.atlassian.upm.SelfUpgradePluginAccessorImpl" />
    
    <rest key="rest" path="/plugins" version="1.0">
        <description>Provides REST resources for the plugin system.</description>
    </rest>

    <servlet-filter name="plugin manager pdk install usurper filter" key="plugin-manager-pdk-usurper" class="com.atlassian.upm.PluginManagerPdkUsurperFilter" location="before-decoration" weight="1">
        <url-pattern>/admin/uploadplugin.action</url-pattern>
    </servlet-filter>

    <servlet-context-listener key="fileCleanup" class="org.apache.commons.fileupload.servlet.FileCleanerCleanup" />

    <servlet key="plugin-manager-servlet" class="com.atlassian.upm.PluginManagerServlet">
        <url-pattern>/upm</url-pattern>
    </servlet>

    <!-- Shared resources -->
    <resource type="i18n" name="i18n" location="com.atlassian.upm.i18n" />

    <resource type="download" name="images/" location="images/"/>

    <component key="auditLogUpgradeTask" name="Audit Log Upgrade Task" class="com.atlassian.upm.log.AuditLogUpgradeTask" public="true">
        <interface>com.atlassian.sal.api.upgrade.PluginUpgradeTask</interface>
    </component>

</atlassian-plugin>
