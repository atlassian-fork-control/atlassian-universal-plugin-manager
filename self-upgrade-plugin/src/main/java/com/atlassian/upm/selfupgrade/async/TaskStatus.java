package com.atlassian.upm.selfupgrade.async;

import java.net.URI;

import com.atlassian.upm.selfupgrade.rest.representations.TaskStatusRepresentation;

public abstract class TaskStatus
{
    private final String source;
    
    protected TaskStatus(String source)
    {
        this.source = source;
    }
    
    public abstract boolean isDone();
    
    public abstract boolean isError();
    
    public String getSource()
    {
        return source;
    }
    
    public abstract TaskStatusRepresentation getRepresentation();
    
    public static TaskStatus inProgress(String source)
    {
        return new InProgressStatus(source);
    }
    
    public static TaskStatus success(URI stubDeletionUri, String source)
    {
        return new SuccessStatus(stubDeletionUri, source);
    }
    
    public static TaskStatus error(String message, String source)
    {
        return new ErrorStatus(message, source);
    }
    
    private static class InProgressStatus extends TaskStatus
    {
        InProgressStatus(String source)
        {
            super(source);
        }
        
        @Override
        public boolean isDone()
        {
            return false;
        }
        
        @Override
        public boolean isError()
        {
            return false;
        }
        
        @Override
        public TaskStatusRepresentation getRepresentation()
        {
            return TaskStatusRepresentation.inProgress(getSource());
        }
    }

    private static class SuccessStatus extends TaskStatus
    {
        private final URI stubDeletionUri;
        
        SuccessStatus(URI stubDeletionUri, String source)
        {
            super(source);
            this.stubDeletionUri = stubDeletionUri;
        }
        
        @Override
        public boolean isDone()
        {
            return true;
        }
        
        @Override
        public boolean isError()
        {
            return false;
        }
        
        @Override
        public TaskStatusRepresentation getRepresentation()
        {
            return TaskStatusRepresentation.success(stubDeletionUri, getSource());
        }
    }

    private static class ErrorStatus extends TaskStatus
    {
        private final String message;
        
        ErrorStatus(String message, String source)
        {
            super(source);
            this.message = message;
        }
        
        @Override
        public boolean isDone()
        {
            return true;
        }
        
        @Override
        public boolean isError()
        {
            return true;
        }
        
        @Override
        public TaskStatusRepresentation getRepresentation()
        {
            return TaskStatusRepresentation.error(message, getSource());
        }
    }
}
