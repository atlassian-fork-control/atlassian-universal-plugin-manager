package com.atlassian.upm.selfupgrade.rest.representations;

import java.net.URI;

import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.upm.selfupgrade.rest.representations.MediaTypes.INSTALL_ERR_JSON;

import static com.atlassian.upm.selfupgrade.rest.representations.MediaTypes.INSTALL_NEXT_TASK_JSON;

import static com.atlassian.upm.selfupgrade.rest.representations.MediaTypes.INSTALL_INSTALLING_JSON;

public class TaskStatusRepresentation
{
    private final int statusCode;
    private final String contentType;
    @JsonProperty private final String source;

    protected TaskStatusRepresentation(int statusCode, String contentType, String source)
    {
        this.statusCode = statusCode;
        this.contentType = contentType;
        this.source = source;
    }
    
    public int getStatusCode()
    {
        return statusCode;
    }
    
    public String getContentType()
    {
        return contentType;
    }

    public String getSource()
    {
        return source;
    }
    
    public static TaskStatusRepresentation inProgress(String source)
    {
        return new InProgressRepresentation(source);
    }
    
    static class InProgressRepresentation extends TaskStatusRepresentation
    {
        InProgressRepresentation(String source)
        {
            super(Status.OK.getStatusCode(), INSTALL_INSTALLING_JSON, source);
        }
    }

    public static TaskStatusRepresentation success(URI installedPluginUri, String source)
    {
        return new SuccessRepresentation(installedPluginUri, source);
    }
    
    static class SuccessRepresentation extends TaskStatusRepresentation
    {
        @JsonProperty URI cleanupDeleteUri;
        @JsonProperty Boolean requiresRefresh;
        
        SuccessRepresentation(URI cleanupDeleteUri, String source)
        {
            super(Status.ACCEPTED.getStatusCode(), INSTALL_NEXT_TASK_JSON, source);
            this.cleanupDeleteUri = cleanupDeleteUri;
            
            // setting status.requiresRefresh tells the front end to display a "you should
            // refresh the browser because UPM has been upgraded" banner.
            this.requiresRefresh = true;
        }
        
        public URI getCleanupDeleteUri()
        {
            return cleanupDeleteUri;
        }
        
        public Boolean getRequiresRefresh()
        {
            return requiresRefresh;
        }
    }

    public static TaskStatusRepresentation error(String message, String source)
    {
        return new ErrorRepresentation(message, source);
    }
    
    static class ErrorRepresentation extends TaskStatusRepresentation
    {
        @JsonProperty String subCode;
        
        ErrorRepresentation(String subCode, String source)
        {
            super(Status.OK.getStatusCode(), INSTALL_ERR_JSON, source);
            this.subCode = subCode;
        }
        
        public String getSubCode()
        {
            return subCode;
        }
    }
}
