package com.atlassian.upm.selfupgrade.rest.representations;

public abstract class MediaTypes
{
    public static final String INSTALL_INSTALLING_JSON = "application/vnd.atl.plugins.install.installing+json";
    
    public static final String INSTALL_NEXT_TASK_JSON = "application/vnd.atl.plugins.install.next-task+json";
    
    public static final String INSTALL_ERR_JSON = "application/vnd.atl.plugins.task.install.err+json";
    
    public static final String PENDING_TASK_JSON = "application/vnd.atl.plugins.pending-task+json";
}
