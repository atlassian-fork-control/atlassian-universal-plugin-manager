package com.atlassian.upm.selfupgrade.rest.resources;

import java.util.concurrent.Callable;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginException;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.upm.selfupgrade.UpgradeParametersFactory;
import com.atlassian.upm.selfupgrade.UpgradeParameters;
import com.atlassian.upm.selfupgrade.async.SimpleAsyncTaskManager;
import com.atlassian.upm.selfupgrade.async.TaskStatus;
import com.atlassian.upm.selfupgrade.rest.representations.TaskRepresentation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.upm.selfupgrade.rest.representations.MediaTypes.PENDING_TASK_JSON;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;

@WebSudoRequired
@Path("/")
public class ExecuteUpgradeResource
{
    private static final Logger logger = LoggerFactory.getLogger(ExecuteUpgradeResource.class);
    
    private static final String INSTALL_TYPE = "PLUGIN_INSTALL";
    private static final int DEFAULT_POLL_DELAY_MS = 100;

    private final SimpleAsyncTaskManager simpleAsyncTaskManager;
    private final PluginController pluginController;
    private final UpgradeParametersFactory configuration;
    private final TransactionTemplate txTemplate;
    
    public ExecuteUpgradeResource(PluginController pluginController,
                                  UpgradeParametersFactory configuration,
                                  SimpleAsyncTaskManager simpleAsyncTaskManager,
                                  TransactionTemplate txTemplate)
    {
        this.pluginController = checkNotNull(pluginController, "pluginController");
        this.configuration = checkNotNull(configuration, "configuration");
        this.simpleAsyncTaskManager = checkNotNull(simpleAsyncTaskManager, "simpleAsyncTaskManager");
        this.txTemplate = checkNotNull(txTemplate, "txTemplate");
    }
    
    @POST
    @Produces(PENDING_TASK_JSON)
    public Response execute()
    {
        final UpgradeParameters params = configuration.getUpgradeParameters();
        if (params == null)
        {
            logger.warn("Upgrade resource was called without being preconfigured; ignoring request");
            return Response.status(Status.FORBIDDEN).build();
        }
        
        final JarPluginArtifact artifact = new JarPluginArtifact(params.getJarToInstall());
        final String source = params.getJarToInstall().getName();
        
        configuration.reset();

        Callable<TaskStatus> installHandler = new Callable<TaskStatus>()
        {
            public TaskStatus call() throws Exception
            {
                String installedPluginKey = installPlugin(artifact);
                
                if (!installedPluginKey.equals(params.getExpectedPluginKey()))
                {
                    String message = "Upgrade task installed plugin with key \"" + installedPluginKey
                                + "\", but the expected key was \"" + params.getExpectedPluginKey() + "\"";
                    logger.warn(message);
                    return TaskStatus.error(message, source);
                }
                
                return TaskStatus.success(params.getSelfUpgradePluginUri(), source);
            }
        };
        
        TaskRepresentation taskRep = simpleAsyncTaskManager.start(installHandler, INSTALL_TYPE, source, DEFAULT_POLL_DELAY_MS)
            .getRepresentation();
        
        return Response.status(Status.CREATED)
            .entity(taskRep)
            .type(PENDING_TASK_JSON)
            .location(taskRep.getSelf())
            .build();
    }
    
    private String installPlugin(final PluginArtifact pluginArtifact)
    {
        return (String) txTemplate.execute(new TransactionCallback<String>()
        {
            public String doInTransaction()
            {
                String pluginKey = getOnlyElement(pluginController.installPlugins(pluginArtifact));

                if (pluginKey == null)
                {
                    throw new PluginException("Plugin failed to install: " + pluginArtifact.getName());
                }

                return pluginKey;
            }
        });
    }
}
