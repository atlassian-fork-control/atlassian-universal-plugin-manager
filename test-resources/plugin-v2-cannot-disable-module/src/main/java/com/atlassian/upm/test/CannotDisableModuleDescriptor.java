package com.atlassian.upm.test;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.CannotDisable;

@CannotDisable
public class CannotDisableModuleDescriptor extends AbstractModuleDescriptor
{
    @Override
    public Object getModule()
    {
        return null;
    }
}