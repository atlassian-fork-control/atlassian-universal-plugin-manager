package com.atlassian.upm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.antlr.stringtemplate.StringTemplate;

public class ExampleServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        StringTemplate stringTemplate = new StringTemplate("Hello, $name$");
        stringTemplate.setAttribute("name", "bob");
        res.getWriter().write(stringTemplate.toString());
        res.getWriter().close();
    }
}
