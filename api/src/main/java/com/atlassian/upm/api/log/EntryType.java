package com.atlassian.upm.api.log;

/**
 * Represents various types of audit log entries.
 *
 * @since 1.6
 */
public enum EntryType
{
    PLUGIN_INSTALL("upm.auditLog.entryType.pluginInstall", "upm.auditLog.install"),
    PLUGIN_UNINSTALL("upm.auditLog.entryType.pluginUninstall", "upm.auditLog.uninstall"),
    PLUGIN_ENABLE("upm.auditLog.entryType.pluginEnable", "upm.auditLog.enable"),
    PLUGIN_DISABLE("upm.auditLog.entryType.pluginDisable", "upm.auditLog.disable"),
    ENTER_SAFE_MODE("upm.auditLog.entryType.enterSafeMode", "upm.auditLog.safeMode.enter"),
    EXIT_SAFE_MODE("upm.auditLog.entryType.exitSafeMode", "upm.auditLog.safeMode.exit"),
    SYSTEM_STARTUP("upm.auditLog.entryType.systemStartup", "upm.auditLog.system.startup"),
    CANCELLED_CHANGE("upm.auditLog.entryType.cancelledChange", "upm.auditLog.cancelChange"),
    UNCLASSIFIED_EVENT("upm.auditLog.entryType.unclassified", null);

    private final String i18nName;
    private final String i18nPrefix;

    private EntryType(String i18nName, String i18nPrefix)
    {
        this.i18nName = i18nName;
        this.i18nPrefix = i18nPrefix;
    }

    /**
     * Returns a i18n key for a displayable name.
     * @return a i18n key for a displayable name.
     */
    public String getI18nName()
    {
        return i18nName;
    }

    /**
     * Returns the i18n key prefix for which all audit log entries of this type will start with.
     * @return the i18n key prefix for which all audit log entries of this type will start with.
     */
    private String getI18nPrefix()
    {
        return i18nPrefix;
    }

    /**
     * Returns the {@link EntryType} associated with the given i18n key
     * @param i18n the i18n
     * @return the {@link EntryType} associated with the given i18n key
     */
    public static EntryType valueOfI18n(String i18n)
    {
        for (EntryType type : values())
        {
            if (type.getI18nPrefix() != null && i18n.startsWith(type.getI18nPrefix()))
            {
                return type;
            }
        }

        return UNCLASSIFIED_EVENT;
    }
}
