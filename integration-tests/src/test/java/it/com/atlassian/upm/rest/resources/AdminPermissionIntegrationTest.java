package it.com.atlassian.upm.rest.resources;

import java.net.URISyntaxException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.test.RestTester;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.codehaus.jettison.json.JSONException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.UpmTestGroups.BAMBOO;
import static com.atlassian.upm.test.UpmTestGroups.FECRU;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Verifies the default {@code PermissionService}'s functionality, including
 * that sysadmins have more access than admins.
 */
public class AdminPermissionIntegrationTest extends UpmResourceTestBase
{
    private static RestTester adminRestTester;

    @BeforeClass
    public static void setUpAdminRestTester()
    {
        adminRestTester = new RestTester("betty", "betty");
    }

    @AfterClass
    public static void destroyAdminRestTester()
    {
        adminRestTester.destroy();
    }

    @Test
    @TestGroups(excludes = {BAMBOO}, reason = "Bamboo has no notion of SysAdmin vs Admin")
    public void installPluginIsDisallowedForAdmin() throws JSONException
    {
        try
        {
            ClientResponse response = adminRestTester.installPluginAndWaitForCompletion(INSTALLABLE);
            assertThat(response.getResponseStatus(), is(UNAUTHORIZED));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    @TestGroups(excludes = {BAMBOO}, reason = "Bamboo has no notion of SysAdmin vs Admin")
    public void upgradePluginIsDisallowedForAdmin() throws JSONException
    {
        try
        {
            ClientResponse response = adminRestTester.upgradeAllAndWaitForCompletion();
            assertThat(response.getResponseStatus(), is(UNAUTHORIZED));
        }
        finally
        {
            restTester.uninstallPlugin(INSTALLABLE);
            restTester.uninstallPlugin(CANNOT_DISABLE_MODULE);
        }
    }

    @Test
    @TestGroups(excludes = {BAMBOO}, reason = "Bamboo has no notion of SysAdmin vs Admin")
    public void uninstallPluginIsDisallowedForAdmin() throws JSONException
    {
        try
        {
            restTester.installPluginsAndVerify(INSTALLABLE);
            ClientResponse response = adminRestTester.uninstallPlugin(INSTALLABLE);
            assertThat(response.getResponseStatus(), is(UNAUTHORIZED));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    @TestGroups(excludes = {FECRU}, reason = "UPM-962 - feCru AMPS data does not have the correct users for this test.")
    public void enablePluginIsAllowedForAdmin() throws JSONException, URISyntaxException
    {
        try
        {
            restTester.installPluginsAndVerify(INSTALLABLE);
            restTester.disablePlugin(INSTALLABLE);

            ClientResponse response = adminRestTester.enablePlugin(INSTALLABLE);
            assertThat(response.getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    @TestGroups(excludes = {FECRU}, reason = "UPM-962 - feCru AMPS data does not have the correct users for this test.")
    public void disblePluginIsAllowedForAdmin() throws JSONException, URISyntaxException
    {
        try
        {
            restTester.installPluginsAndVerify(INSTALLABLE);

            ClientResponse response = adminRestTester.disablePlugin(INSTALLABLE);
            assertThat(response.getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    @TestGroups(excludes = {FECRU}, reason = "UPM-962 - feCru AMPS data does not have the correct users for this test.")
    public void getPluginIsAllowedForAdmin() throws JSONException, URISyntaxException
    {
        try
        {
            restTester.installPluginsAndVerify(INSTALLABLE);

            ClientResponse response = adminRestTester.getPlugin(INSTALLABLE, ClientResponse.class);
            assertThat(response.getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }
}