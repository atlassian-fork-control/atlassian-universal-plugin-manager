package it.com.atlassian.upm.rest.resources;

import com.atlassian.upm.rest.representations.ChangesRequiringRestartRepresentation;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmResourceTestBase;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class ChangeRequiringRestartResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatInstallingPluginRequiringRestartAndThenCancellingChangeResultsInEmptyChangeList() throws Exception
    {
        restTester.installPluginAndWaitForCompletion(TestPlugins.INSTALLABLE_REQUIRES_RESTART);
        restTester.deleteChangeRequiringRestart(TestPlugins.INSTALLABLE_REQUIRES_RESTART);
        ChangesRequiringRestartRepresentation changes = restTester.getChangesRequiringRestart();
        assertThat(changes.getChanges(), Matchers.<ChangesRequiringRestartRepresentation.ChangeRepresentation>emptyIterable());
    }
}
