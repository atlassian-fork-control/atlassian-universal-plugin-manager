package it.com.atlassian.upm.rest.resources;

import com.atlassian.upm.rest.representations.PopularPluginCollectionRepresentation;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmResourceTestBase;

import org.junit.Test;

import static com.atlassian.upm.test.UpmIntegrationMatchers.containsPlugins;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PopularPluginCollectionResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatPopularPluginsResourceContainsAllPlugins()
    {
        PopularPluginCollectionRepresentation popularPlugins = restTester.getPopularPluginCollection();
        assertThat(popularPlugins.getPlugins(), containsPlugins(
            TestPlugins.INSTALLABLE,
            TestPlugins.OBR,
            TestPlugins.NONDEPLOYABLE,
            TestPlugins.USER_INSTALLED_WITH_MODULES,
            TestPlugins.NOT_ENABLED_BY_DEFAULT
        ));
    }

    @Test
    public void assertThatPopularPluginsResourceContainsSameAmountAsMaxResults()
    {
        PopularPluginCollectionRepresentation popularPlugins = restTester.getPopularPluginCollection(1, 0);
        assertThat(popularPlugins.getPlugins(), containsPlugins(
            TestPlugins.INSTALLABLE
        ));
    }

    @Test
    public void assertThatPopularPluginsResourceStartsWithSpecifiedStartIndex()
    {
        PopularPluginCollectionRepresentation popularPlugins = restTester.getPopularPluginCollection(0, 1);
        assertThat(popularPlugins.getPlugins(), containsPlugins(
            TestPlugins.OBR,
            TestPlugins.NONDEPLOYABLE,
            TestPlugins.USER_INSTALLED_WITH_MODULES,
            TestPlugins.NOT_ENABLED_BY_DEFAULT
        ));
    }

    @Test
    public void assertThatPopularPluginsHasSafeModeFalseWhenNotInSafeMode()
    {
        PopularPluginCollectionRepresentation plugins = restTester.getPopularPluginCollection();
        assertFalse(plugins.isSafeMode());
    }

    @Test
    public void assertThatPopularPluginsHasSafeModeTrueWhenInSafeMode()
    {
        restTester.enterSafeMode();
        try
        {
            PopularPluginCollectionRepresentation plugins = restTester.getPopularPluginCollection();
            assertTrue(plugins.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }
}