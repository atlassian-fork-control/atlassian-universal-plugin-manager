package it.com.atlassian.upm.rest.resources;

import java.util.Iterator;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.rest.representations.InstalledPluginCollectionRepresentation;
import com.atlassian.upm.test.AsynchronousTaskException;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.codehaus.jettison.json.JSONException;
import org.junit.Ignore;
import org.junit.Test;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_REQUIRES_RESTART;
import static com.atlassian.upm.test.TestPlugins.LEGACY_PLUGIN;
import static com.atlassian.upm.test.TestPlugins.NONEXISTENT_JAR;
import static com.atlassian.upm.test.TestPlugins.OBR;
import static com.atlassian.upm.test.TestPlugins.OBR_DEP;
import static com.atlassian.upm.test.TestPlugins.OSGI_BUNDLE;
import static com.atlassian.upm.test.TestPlugins.STATIC;
import static com.atlassian.upm.test.TestPlugins.SYSTEM;
import static com.atlassian.upm.test.TestPlugins.UNSUPPORTED_PLUGIN;
import static com.atlassian.upm.test.TestPlugins.XML_PLUGIN;
import static com.atlassian.upm.test.UpmIntegrationMatchers.hasPlugins;
import static com.atlassian.upm.test.UpmIntegrationMatchers.hasSubCode;
import static com.atlassian.upm.test.UpmIntegrationMatchers.systemPlugin;
import static com.atlassian.upm.test.UpmIntegrationMatchers.userInstalledPlugin;
import static com.atlassian.upm.test.UpmIntegrationMatchers.withRestartState;
import static com.atlassian.upm.test.UpmTestGroups.BAMBOO;
import static com.atlassian.upm.test.UpmTestGroups.CONFLUENCE;
import static com.atlassian.upm.test.UpmTestGroups.FECRU;
import static com.atlassian.upm.test.UpmTestGroups.REFAPP;
import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class InstalledPluginCollectionResourceIntegrationTest extends UpmResourceTestBase
{
    private static final String TEST_PLUGIN_PATH = "atlassian-universal-plugin-manager-test-plugin-v2-installable.jar";

    @Test
    public void installPluginWithInvalidUriReturnsBadRequest() throws JSONException
    {
        ClientResponse response = restTester.installPluginAndWaitForCompletion("bad url");
        assertThat(response.getResponseStatus(), is(BAD_REQUEST));
    }

    @Test(expected = AsynchronousTaskException.class)
    public void installPluginWithUnknownPluginTypeReturnsUnknownPluginTypeError() throws JSONException
    {
        String uri = getStandardApplicationProperties().getBaseUrl() + "/rest/fakepac/1.0/nonplugin";
        try
        {
            restTester.installPluginAndWaitForCompletion(uri);
        }
        catch (AsynchronousTaskException ate)
        {
            checkExceptionMessageAndRethrow(ate, "upm.pluginInstall.error.unknown.plugin.type");
        }
    }

    @Test(expected = AsynchronousTaskException.class)
    public void installPluginWithFileProtocolReturnsUnsupportedProtocolError() throws JSONException
    {
        String uri = "file:///om.nom.nom";
        try
        {
            restTester.installPluginAndWaitForCompletion(uri);
        }
        catch (AsynchronousTaskException ate)
        {
            checkExceptionMessageAndRethrow(ate, "upm.pluginInstall.error.unsupported.protocol");
        }
    }

    @Test
    public void installPluginWithRelativeUriReturnsRelativeUriError() throws JSONException
    {
        String uri = "giggity";
        ClientResponse response = restTester.installPluginAndWaitForCompletion(uri);
        assertThat(getErrorRepresentationFromResponse(response), hasSubCode("upm.pluginInstall.error.invalid.relative.uri"));
    }

    @Test(expected = AsynchronousTaskException.class)
    public void installXmlPluginWithNoValidAtlassianPluginsDescriptorReturnsUnknownPluginTypeError() throws JSONException
    {
        String uri = getStandardApplicationProperties().getBaseUrl() + "/rest/fakepac/1.0/xmlplugins/product-upgrades.xml";
        try
        {
            restTester.installPluginAndWaitForCompletion(uri);
        }
        catch (AsynchronousTaskException ate)
        {
            checkExceptionMessageAndRethrow(ate, "upm.pluginInstall.error.unknown.plugin.type");
        }
    }

    @Test(expected = AsynchronousTaskException.class)
    public void installPluginWithFakeUriReturnsResponseException() throws JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(NONEXISTENT_JAR);
        }
        catch (AsynchronousTaskException ate)
        {
            checkExceptionMessageAndRethrow(ate, "upm.pluginInstall.error.response.exception");
        }
    }

    @Test
    public void installPluginWithValidJarUri() throws JSONException
    {
        try
        {
            checkNotInstalled(INSTALLABLE);

            ClientResponse response = restTester.installPluginAndWaitForCompletion(INSTALLABLE.getDownloadUri(restTester.getBaseUri()));
            assertThat(response.getResponseStatus(), is(OK));
            assertThat(restTester.getPlugin(INSTALLABLE.getKey(), ClientResponse.class).getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE.getKey());
        }
    }

    @Test
    public void installPluginWithValidJarUriAndCheckInitialResponse() throws JSONException
    {
        try
        {
            checkNotInstalled(INSTALLABLE);

            ClientResponse response = restTester.installPluginAndReturnAcceptedResponse(INSTALLABLE.getDownloadUri(restTester.getBaseUri()));
            assertThat(response.getResponseStatus(), is(ACCEPTED));
            restTester.waitForCompletion(response, String.class);
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE.getKey());
        }
    }

    @Test
    public void installPluginWithValidObrUri() throws JSONException
    {
        try
        {
            checkNotInstalled(OBR);
            checkNotInstalled(OBR_DEP);

            ClientResponse response = restTester.installPluginAndWaitForCompletion(OBR.getDownloadUri(restTester.getBaseUri()));
            assertThat(response.getResponseStatus(), is(OK));
            assertThat(restTester.getPlugin(OBR.getKey(), ClientResponse.class).getResponseStatus(), is(OK));
            assertThat(restTester.getPlugin(OBR_DEP.getKey(), ClientResponse.class).getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(OBR.getKey());
            restTester.uninstallPluginAndVerify(OBR_DEP.getKey());
        }
    }

    @Test
    public void installBundle() throws JSONException
    {
        try
        {
            checkNotInstalled(OSGI_BUNDLE);

            ClientResponse response = restTester.installPluginAndWaitForCompletion(OSGI_BUNDLE.getDownloadUri(restTester.getBaseUri()));
            assertThat(response.getResponseStatus(), is(OK));
            assertThat(restTester.getPlugin(OSGI_BUNDLE.getKey(), ClientResponse.class).getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(OSGI_BUNDLE.getKey());
        }
    }

    public void installPluginFailsAndReturnsConflictWhenInSafeMode() throws JSONException
    {
        restTester.enterSafeMode();
        try
        {
            assertThat(restTester.installPluginAndWaitForCompletion(INSTALLABLE.getDownloadUri(restTester.getBaseUri())).getStatus(), is(equalTo(CONFLICT.getStatusCode())));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    @TestGroups(excludes = {BAMBOO}, reason = "UPM-1098 - Upload plugin from file system is not working in Bamboo 3.1")
    public void installPluginFromFileSystem() throws JSONException
    {
        try
        {
            checkNotInstalled(INSTALLABLE);

            // install the plugin
            restTester.installPluginFromFileSystemAndWaitForCompletion(TEST_PLUGIN_PATH);

            // assert that the plugin has been installed and can be retrieved
            ClientResponse response = restTester.getPlugin(INSTALLABLE.getKey(), ClientResponse.class);
            assertThat(response.getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE.getKey());
        }
    }

    @Test
    public void installPluginFromFileSystemFailsAndReturnsConflictWhenInSafeMode() throws JSONException
    {
        restTester.enterSafeMode();
        try
        {
            assertThat(restTester.installPluginFromFileSystemAndWaitForCompletion(TEST_PLUGIN_PATH).getStatus(), is(equalTo(CONFLICT.getStatusCode())));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void installPluginFromFileSystemFailsAndReturnsForbiddenWhenNoTokenIsIncluded() throws JSONException
    {
        assertThat(restTester.installPluginFromFileSystemAndWaitForCompletion(TEST_PLUGIN_PATH, false).getStatus(), is(equalTo(FORBIDDEN.getStatusCode())));
    }

    @Test
    public void verifySystemPluginInPluginCollection()
    {
        InstalledPluginCollectionRepresentation plugins = restTester.getInstalledPluginCollection();
        assertThat(plugins.getPlugins(), hasPlugins(systemPlugin(SYSTEM.getEscapedKey())));
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye doesn't support static plugins")
    public void verifyUserInstalledStaticPluginInPluginCollection()
    {
        InstalledPluginCollectionRepresentation plugins = restTester.getInstalledPluginCollection();
        assertThat(plugins.getPlugins(), hasPlugins(userInstalledPlugin(STATIC.getEscapedKey())));
    }

    @Test
    public void assertThatInstalledPluginsHasSafeModeFalseWhenNotInSafeMode()
    {
        InstalledPluginCollectionRepresentation plugins = restTester.getInstalledPluginCollection();
        assertFalse(plugins.isSafeMode());
    }

    @Test
    public void assertThatInstalledPluginsHasSafeModeTrueWhenInSafeMode()
    {
        restTester.enterSafeMode();
        try
        {
            InstalledPluginCollectionRepresentation plugins = restTester.getInstalledPluginCollection();
            assertTrue(plugins.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void assertThatInstalledPluginsAreSortedByAlpha()
    {
        InstalledPluginCollectionRepresentation plugins = restTester.getInstalledPluginCollection();
        final Iterator<InstalledPluginCollectionRepresentation.PluginEntry> pluginIterator = plugins.getPlugins().iterator();

        // It is safe to assume that there are at least 3 plugins installed in each test env
        final InstalledPluginCollectionRepresentation.PluginEntry pluginEntry1 = pluginIterator.next();
        final InstalledPluginCollectionRepresentation.PluginEntry pluginEntry2 = pluginIterator.next();
        final InstalledPluginCollectionRepresentation.PluginEntry pluginEntry3 = pluginIterator.next();
        // Check that the first few plugins are alphabetically in the order we expect        
        assertTrue(pluginEntry1.getName().compareTo(pluginEntry2.getName()) < 0);
        assertTrue(pluginEntry1.getName().compareTo(pluginEntry3.getName()) < 0);
        assertTrue(pluginEntry2.getName().compareTo(pluginEntry3.getName()) < 0);
    }

    @Test
    @Ignore("Until AMPS-277 is fixed and we work out how to cancel installs this leaves the plugin installed and the state cannot be reverted")
    public void installedPluginThatRequiresRestartHasCorrectRestartState() throws Exception
    {
        restTester.installPluginAndWaitForCompletion(INSTALLABLE_REQUIRES_RESTART);
        try
        {
            InstalledPluginCollectionRepresentation plugins = restTester.getInstalledPluginCollection();
            assertThat(plugins.getPlugins(), hasPlugins(userInstalledPlugin(INSTALLABLE_REQUIRES_RESTART, withRestartState("install"))));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE_REQUIRES_RESTART);
        }
    }

    private static final String TEST_LEGACY_PLUGIN_PATH = "atlassian-universal-plugin-manager-test-plugin-v1-installable.jar";

    @Test
    @TestGroups(value = CONFLUENCE, reason = "Only Confluence supports dynamically installing legacy plugins.")
    public void installLegacyPluginWithValidJarUri() throws JSONException
    {
        try
        {
            restTester.ensureNotInstalled(LEGACY_PLUGIN);

            ClientResponse response = restTester.installPluginAndWaitForCompletion(LEGACY_PLUGIN);
            assertThat(response.getResponseStatus(), is(OK));
            assertThat(restTester.getPlugin(LEGACY_PLUGIN, ClientResponse.class).getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(LEGACY_PLUGIN);
        }
    }

    @Test
    @TestGroups(value = CONFLUENCE, reason = "Only Confluence supports dynamically installing legacy plugins.")
    public void installLegacyPluginFromFileSystem() throws JSONException
    {
        try
        {
            restTester.ensureNotInstalled(LEGACY_PLUGIN);

            // install the plugin
            restTester.installPluginFromFileSystemAndWaitForCompletion(TEST_LEGACY_PLUGIN_PATH);

            // assert that the plugin has been installed and can be retrieved
            ClientResponse response = restTester.getPlugin(LEGACY_PLUGIN, ClientResponse.class);
            assertThat(response.getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(LEGACY_PLUGIN);
        }
    }

    @Test
    @TestGroups(excludes = CONFLUENCE, reason = "Only Confluence supports dynamically installing legacy plugins, " +
        "everything else should return the appropriate error subcode.")
    public void installingLegacyPluginByUriFailsWithLegayPluginsUnsupportedSubcode() throws Exception
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(LEGACY_PLUGIN);
        }
        catch (AsynchronousTaskException ate)
        {
            assertThat(ate.getMessage(), is(equalTo("upm.pluginInstall.error.legacy.plugins.unsupported")));
        }
    }

    @Test
    @TestGroups(excludes = CONFLUENCE, reason = "Only Confluence supports dynamically installing legacy plugins, " +
        "everything else should return the appropriate error subcode.")
    public void installingLegacyPluginByFileUploadFailsWithLegayPluginsUnsupportedSubcode() throws Exception
    {
        try
        {
            restTester.installPluginFromFileSystemAndWaitForCompletion(TEST_LEGACY_PLUGIN_PATH);
        }
        catch (AsynchronousTaskException ate)
        {
            assertThat(ate.getMessage(), is(equalTo("upm.pluginInstall.error.legacy.plugins.unsupported")));
        }
    }

    @Test(expected=AsynchronousTaskException.class)
    public void installingUnrecognisedPluginVersionByUriFailsWithCorrectSubcode() throws Exception
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(UNSUPPORTED_PLUGIN);
        }
        catch (AsynchronousTaskException ate)
        {
            checkExceptionMessageAndRethrow(ate, "upm.pluginInstall.error.unrecognised.plugin.version");
        }
    }

    private static final String TEST_UNSUPPORTED_PLUGIN_PATH = "atlassian-universal-plugin-manager-test-plugin-v99-unsupported.jar";
    
    @Test(expected=AsynchronousTaskException.class)
    @TestGroups(excludes = {BAMBOO}, reason = "UPM-1098 - Upload plugin from file system is not working in Bamboo 3.1")
    public void installingUnrecognisedPluginVersionByFileUploadFailsWithCorrectSubcode() throws Exception
    {
        try
        {
            restTester.installPluginFromFileSystemAndWaitForCompletion(TEST_UNSUPPORTED_PLUGIN_PATH);
        }
        catch (AsynchronousTaskException ate)
        {
            checkExceptionMessageAndRethrow(ate, "upm.pluginInstall.error.unrecognised.plugin.version");
        }
    }    
    
    @Test
    @TestGroups(excludes={REFAPP, FECRU}, reason="Only JIRA, Confluence and Bamboo supports dynamically installing " +
                "xml plugins.")
    public void installXmlPluginWithValidXmlUri() throws JSONException
    {
        try
        {
            restTester.ensureNotInstalled(XML_PLUGIN);

            ClientResponse response = restTester.installPluginAndWaitForCompletion(XML_PLUGIN);
            assertThat(response.getResponseStatus(), is(OK));
            assertThat(restTester.getPlugin(XML_PLUGIN, ClientResponse.class).getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(XML_PLUGIN);
        }
    }

    @Test
    @TestGroups(value = {REFAPP, FECRU}, reason = "Only JIRA, Confluence and Bamboo supports dynamically installing " +
        "xml plugins, everything else should return the appropriate error subcode.")
    public void installingXmlPluginByUriFailsWithXmlPluginsUnsupportedSubcode() throws Exception
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(XML_PLUGIN);
        }
        catch (AsynchronousTaskException ate)
        {
            assertThat(ate.getMessage(), is(equalTo("upm.pluginInstall.error.xml.plugins.unsupported")));
        }
    }

    private void checkExceptionMessageAndRethrow(RuntimeException exn, String message)
    {
        assertThat(exn.getMessage(), is(equalTo(message)));
        throw exn;
    }
}
