package it.com.atlassian.upm.rest.resources;

import java.net.URISyntaxException;

import com.atlassian.upm.rest.representations.InstalledPluginCollectionRepresentation;
import com.atlassian.upm.rest.representations.InstalledPluginCollectionRepresentation.PluginEntry;
import com.atlassian.upm.rest.representations.PluginModuleRepresentation;
import com.atlassian.upm.rest.representations.PluginRepresentation;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.USER_INSTALLED_WITH_MODULES;
import static com.google.common.base.Preconditions.checkState;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SafeModeResourceIntegrationTest extends UpmResourceTestBase
{
    private static final String USER_INSTALLED_MODULE_KEY = "some_module";

    @Before
    public void installTestPlugin() throws Exception
    {
        restTester.installPluginsAndVerify(INSTALLABLE);
    }

    @After
    public void uninstallTestPlugin()
    {
        restTester.uninstallPluginAndVerify(INSTALLABLE);
    }

    @Test
    public void verifySafeModeIsFalse()
    {
        assertFalse(restTester.isSafeMode());
    }

    @Test
    public void verifySafeModeIsTrueWhenSafeModeIsTriggered()
    {
        try
        {
            restTester.enterSafeMode();

            assertTrue(restTester.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void verifyThatTriggeringSafeModeWhileInSafeModeReturnsConflict()
    {
        try
        {
            restTester.enterSafeMode();

            ClientResponse response = restTester.enterSafeMode();
            assertThat(response.getStatus(), is(equalTo(CONFLICT.getStatusCode())));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void verifySafeModeIsFalseWhenSafeModeIsExitedAndConfigurationRestored()
    {
        try
        {
            // make sure we are in safe mode
            restTester.enterSafeMode();
            assertTrue(restTester.isSafeMode());

            // exit safe mode and restore previous configuration
            restTester.exitSafeModeAndRestoreSavedConfiguration();
            assertFalse(restTester.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void verifySafeModeIsFalseWhenSafeModeIsExitedAndCurrentStateIsKept() throws URISyntaxException
    {
        InstalledPluginCollectionRepresentation previousPlugins = restTester.getInstalledPluginCollection();

        try
        {
            // make sure we are in safe mode
            restTester.enterSafeMode();
            assertTrue(restTester.isSafeMode());

            // exit safe mode and keep current state
            restTester.exitSafeModeAndKeepState();
            assertFalse(restTester.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();

            // restore back to the state before we run the test
            restorePreviousPluginState(previousPlugins.getPlugins());
        }
    }

    @Test
    public void verifyUserInstalledPluginsAreDisabledWhenSafeModeIsTriggered() throws URISyntaxException
    {
        try
        {
            restTester.enterSafeMode();
            // make sure our plugin has been disabled
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE);
            assertFalse(plugin.isEnabled());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void verifyThatConfigurationIsRestoredWhenSafeModeIsExitedAndConfigurationRestored() throws URISyntaxException
    {
        try
        {
            // trigger safe mode
            restTester.enterSafeMode();

            // exit safe mode and restore saved configuration
            restTester.exitSafeModeAndRestoreSavedConfiguration();

            // make sure our plugin has been restored to its original state (ENABLED)
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE);
            assertTrue(plugin.isEnabled());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void verifyThatPluginModuleConfigurationIsRestoredWhenSafeModeIsExitedAndConfigurationRestored() throws Exception
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(USER_INSTALLED_WITH_MODULES);

            // trigger safe mode
            restTester.enterSafeMode();

            restTester.disablePluginModule(USER_INSTALLED_WITH_MODULES.getKey(), USER_INSTALLED_MODULE_KEY);

            // make sure our plugin has been disabled
            PluginModuleRepresentation module = restTester.getPluginModule(USER_INSTALLED_WITH_MODULES, USER_INSTALLED_MODULE_KEY);
            checkState(!module.isEnabled(), "Plugin module expected to be disabled, but is not");

            // exit safe mode and restore saved configuration
            restTester.exitSafeModeAndRestoreSavedConfiguration();

            module = restTester.getPluginModule(USER_INSTALLED_WITH_MODULES, USER_INSTALLED_MODULE_KEY);
            assertTrue(module.isEnabled());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
            restTester.uninstallPluginAndVerify(USER_INSTALLED_WITH_MODULES);
        }
    }

    @Test
    public void verifyThatCurrentStateIsKeptWhenSafeModeIsExitedAndCurrentStateIsKept() throws URISyntaxException
    {
        InstalledPluginCollectionRepresentation previousPlugins = restTester.getInstalledPluginCollection();

        try
        {
            // trigger safe mode
            restTester.enterSafeMode();

            // exit safe mode and keep current state
            restTester.exitSafeModeAndKeepState();

            // make sure our plugin retains its current state (DISABLED)
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE);
            assertFalse(plugin.isEnabled());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();

            // restore back to the state before we run the test
            restorePreviousPluginState(previousPlugins.getPlugins());
        }
    }

    private void restorePreviousPluginState(Iterable<PluginEntry> plugins) throws URISyntaxException
    {
        for (PluginEntry plugin : plugins)
        {
            if (plugin.isEnabled())
            {
                restTester.enablePlugin(plugin.getSelfLink());
            }
        }
    }
}
