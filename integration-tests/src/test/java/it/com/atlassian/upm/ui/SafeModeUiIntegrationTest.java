package it.com.atlassian.upm.ui;

import java.io.IOException;
import java.net.URISyntaxException;

import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.representations.InstalledPluginCollectionRepresentation;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmUiTestBase;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import be.roam.hue.doj.Doj;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.google.common.base.Preconditions.checkState;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SafeModeUiIntegrationTest extends UpmUiTestBase
{
    UpmUriBuilder uriBuilder = new UpmUriBuilder(getStandardApplicationProperties());

    @BeforeClass
    public static void loginAndGoToAuditLogTab()
    {
        uiTester.logInAs("admin");
    }

    @BeforeClass
    public static void installTestPlugin() throws Exception
    {
        restTester.installPluginsAndVerify(INSTALLABLE);
    }

    @AfterClass
    public static void logoutAndPurgeAuditLog()
    {
        uiTester.logout();
    }

    @AfterClass
    public static void uninstallTestPlugin()
    {
        restTester.uninstallPluginAndVerify(INSTALLABLE);
    }

    @Before
    public void goToManageExistingPage()
    {
        uiTester.refreshPage();
        uiTester.goToUpmPage();
        uiTester.clickManageExistingTab();
    }

    @Test
    public void safeModeDisablesUpgradeAllButton()
    {
        // enter into safe mode, make sure upgrade button is disabled
        restTester.enterSafeMode();
        goToManageExistingPage();

        try
        {
            assertTrue(isUpgradeAllButtonDisabled());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }

    }

    @Test
    public void exitingSafeModeEnablesUpgradeAllButton()
    {
        // enter and exit safe mode
        restTester.enterSafeMode();
        restTester.exitSafeModeAndRestoreSavedConfiguration();

        // make sure it's re-enabled
        goToManageExistingPage();
        assertFalse(isUpgradeAllButtonDisabled());
    }

    @Test
    public void safeModeDisablesUpgradePluginButton() throws IOException
    {
        // enter into safe mode, go to manage list
        restTester.enterSafeMode();
        goToManageExistingPage();


        try
        {
            // expand plugins to expose upgrade button
            clickExpandAllElement();

            // make sure upgrade button is disabled
            assertTrue(isUpgradePluginButtonDisabled());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void exitingSafeModeEnablesUpgradePluginButton() throws IOException
    {
        restTester.enterSafeMode();
        restTester.exitSafeModeAndRestoreSavedConfiguration();
        goToManageExistingPage();

        clickExpandAllElement();
        assertFalse(isUpgradePluginButtonDisabled());
    }

    @Test
    public void enablingSafeModeDisplaysExitSafeModeLinks()
    {
        assertFalse(isUpmContainerInSafeMode());

        // enter into safe mode
        restTester.enterSafeMode();
        goToManageExistingPage();

        try
        {
            assertTrue(isUpmContainerInSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void exitingSafeModeShowsEnableSafeModeLink()
    {
        // enter into safe mode
        restTester.enterSafeMode();
        goToManageExistingPage();

        assertTrue(isUpmContainerInSafeMode());

        // exit safe mode
        restTester.exitSafeModeAndRestoreSavedConfiguration();
        goToManageExistingPage();

        assertFalse(isUpmContainerInSafeMode());
    }

    @Test
    public void enablingSafeModeDisablesUserInstalledPlugins()
    {
        checkState(!isPluginDisabledInUi(INSTALLABLE));

        // enter into safe mode
        restTester.enterSafeMode();
        goToManageExistingPage();

        try
        {
            assertTrue(isPluginDisabledInUi(INSTALLABLE));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void exitingSafeModeAndRestoringPreviousConfiguration()
    {
        // enter into safe mode
        restTester.enterSafeMode();
        goToManageExistingPage();

        // exit safe mode
        restTester.exitSafeModeAndRestoreSavedConfiguration();
        goToManageExistingPage();

        // user installed plugin must be enabled now
        assertFalse(isPluginDisabledInUi(INSTALLABLE));
    }

    @Test
    public void exitingSafeModeAndKeepingCurrentConfiguration() throws URISyntaxException
    {
        InstalledPluginCollectionRepresentation previousPlugins = restTester.getInstalledPluginCollection();

        try
        {
            // enter into safe mode
            restTester.enterSafeMode();
            goToManageExistingPage();

            // exit safe mode
            restTester.exitSafeModeAndKeepState();
            goToManageExistingPage();

            // user installed plugin must remain disabled
            assertTrue(isPluginDisabledInUi(INSTALLABLE));
        }
        finally
        {
            restorePreviousPluginState(previousPlugins.getPlugins());
        }
    }

    private boolean isUpmContainerInSafeMode()
    {
        return uiTester.elementById("upm-container").hasClass("upm-safe-mode");
    }

    private void restorePreviousPluginState(Iterable<InstalledPluginCollectionRepresentation.PluginEntry> plugins) throws URISyntaxException
    {
        for (InstalledPluginCollectionRepresentation.PluginEntry plugin : plugins)
        {
            if (plugin.isEnabled())
            {
                restTester.enablePlugin(plugin.getSelfLink());
            }
        }
    }

    private Doj getPluginDiv(TestPlugins plugin)
    {
        return uiTester.elementById("upm-panel-manage").get("input").withValue(uriBuilder.buildPluginUri(plugin.getKey()).toString()).parent();
    }

    private boolean isPluginDisabledInUi(TestPlugins plugin)
    {
        return getPluginDiv(plugin).hasClass("disabled");
    }
    
    private boolean isUpgradeAllButtonDisabled()
    {
        return uiTester.elementById("upm-upgrade-all").attribute("disabled").equals("disabled");
    }

    private boolean isUpgradePluginButtonDisabled() {
        return uiTester.elementById("upm-available-upgrades").get("button.upm-upgrade", 0).attribute("disabled").equals("disabled");
    }

    private void clickExpandAllElement() throws IOException
    {
        uiTester.elementById("upm-upgrade-plugins").get("a.upm-expand-all").click();
        uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }
}
