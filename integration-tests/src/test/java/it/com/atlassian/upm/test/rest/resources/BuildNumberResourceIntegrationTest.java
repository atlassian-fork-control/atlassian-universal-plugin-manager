package it.com.atlassian.upm.test.rest.resources;

import java.net.URISyntaxException;

import com.atlassian.upm.test.UpmResourceTestBase;
import com.atlassian.upm.test.UpmTestRunner;
import com.atlassian.upm.test.UpmUiTester;

import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.google.common.base.Preconditions.checkState;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(UpmTestRunner.class)
public class BuildNumberResourceIntegrationTest extends UpmResourceTestBase
{
    private static final String BUILD_NUMBER = "test-build-number";
    @Inject private static UpmUiTester uiTester;

    @BeforeClass
    public static void logIn()
    {
        uiTester.logInAs("admin");
    }

    @Before
    public void goToUpmPage()
    {
        uiTester.goToUpmPage();
    }

    @After
    public void resetBuildNumber()
    {
        restTester.resetBuildNumber();
    }

    @Test
    public void verifySettingBuildNumberGetsExpectedValue() throws URISyntaxException
    {
        ClientResponse response = restTester.setBuildNumber(BUILD_NUMBER);
        checkState(response.getResponseStatus().equals(OK));
        String buildNumber = restTester.getBuildNumber();
        assertThat(buildNumber, is(equalTo(BUILD_NUMBER)));
    }

    @Test
    public void verifyResettingBuildNumberSetsActualValue() throws URISyntaxException
    {
        ClientResponse response = restTester.resetBuildNumber();
        checkState(response.getResponseStatus().equals(OK));
        String buildNumber = restTester.getBuildNumber();
        //system property for build number is not set in this java process, must access it through UI
        assertThat(buildNumber, is(equalTo(uiTester.getBuildNumber())));
    }
}
