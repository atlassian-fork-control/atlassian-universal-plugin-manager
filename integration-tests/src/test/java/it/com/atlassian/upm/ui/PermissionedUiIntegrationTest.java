package it.com.atlassian.upm.ui;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.upm.UpmTab;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.spi.Permission;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmUiTestBase;

import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.sun.jersey.api.client.ClientResponse;

import org.codehaus.jettison.json.JSONException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import be.roam.hue.doj.Doj;
import be.roam.hue.doj.MatchType;

import static com.atlassian.upm.UpmTab.AUDIT_LOG_TAB;
import static com.atlassian.upm.UpmTab.COMPATIBILITY_TAB;
import static com.atlassian.upm.UpmTab.INSTALL_TAB;
import static com.atlassian.upm.UpmTab.MANAGE_EXISTING_TAB;
import static com.atlassian.upm.UpmTab.OSGI_TAB;
import static com.atlassian.upm.UpmTab.UPGRADE_TAB;
import static com.atlassian.upm.spi.Permission.GET_AUDIT_LOG;
import static com.atlassian.upm.spi.Permission.GET_AVAILABLE_PLUGINS;
import static com.atlassian.upm.spi.Permission.GET_OSGI_STATE;
import static com.atlassian.upm.spi.Permission.GET_PLUGIN_MODULES;
import static com.atlassian.upm.spi.Permission.GET_PRODUCT_UPGRADE_COMPATIBILITY;
import static com.atlassian.upm.spi.Permission.MANAGE_AUDIT_LOG;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_ENABLEMENT;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_INSTALL;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_MODULE_ENABLEMENT;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_UNINSTALL;
import static com.atlassian.upm.spi.Permission.MANAGE_SAFE_MODE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.REFIMPL_SPI_PLUGIN;
import static com.atlassian.upm.test.TestPlugins.USER_INSTALLED_WITH_MODULES;
import static com.google.common.base.Preconditions.checkState;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

//annotation is commented out as part of UPM-831
//@TestGroups(value=REFAPP, reason="REST-133, only refapp is on sufficient REST version")
public class PermissionedUiIntegrationTest extends UpmUiTestBase
{
    private static final UpmUriBuilder uriBuilder = new UpmUriBuilder(ApplicationPropertiesImpl.getStandardApplicationProperties());

    @BeforeClass
    public static void login()
    {
        uiTester.logInAs("admin");
    }

    @BeforeClass
    public static void enableRefimplSpi() throws URISyntaxException
    {
        ClientResponse response = restTester.enablePlugin(REFIMPL_SPI_PLUGIN);
        checkState(response.getResponseStatus().equals(OK));
        restTester.addAllPermissions();
    }

    @AfterClass
    public static void disableRefimplSpi() throws URISyntaxException
    {
        restTester.resetPermissions();
        ClientResponse response = restTester.disablePlugin(REFIMPL_SPI_PLUGIN);
        checkState(response.getResponseStatus().equals(OK));
    }

    @AfterClass
    public static void logout()
    {
        uiTester.logout();
    }

    @Test
    public void testLogTabIsVisibleWhenPermitted()
    {
        verifyTabExistsWhenPermitted(AUDIT_LOG_TAB);
    }

    @Test
    public void testLogTabIsHiddenWhenUnpermitted()
    {
        verifyTabDoesNotExistWhenUnpermitted(AUDIT_LOG_TAB, GET_AUDIT_LOG);
    }

    @Test
    public void testCompatibilityTabIsVisibleWhenPermitted()
    {
        verifyTabExistsWhenPermitted(COMPATIBILITY_TAB);
    }

    @Test
    public void testCompatibilityTabIsHiddenWhenUnpermitted()
    {
        verifyTabDoesNotExistWhenUnpermitted(COMPATIBILITY_TAB, GET_PRODUCT_UPGRADE_COMPATIBILITY);
    }

    @Test
    public void testUpgradeTabIsVisibleWhenPermitted()
    {
        verifyTabExistsWhenPermitted(UPGRADE_TAB);
    }

    @Test
    public void testUpgradeTabIsHiddenWhenUnpermitted()
    {
        verifyTabDoesNotExistWhenUnpermitted(UPGRADE_TAB, GET_AVAILABLE_PLUGINS);
    }

    @Test
    public void testInstallTabIsVisibleWhenPermitted()
    {
        verifyTabExistsWhenPermitted(INSTALL_TAB);
    }

    @Test
    public void testInstallTabIsHiddenWhenUnpermitted()
    {
        verifyTabDoesNotExistWhenUnpermitted(INSTALL_TAB, GET_AVAILABLE_PLUGINS);
    }

    @Test
    public void testOsgiTabIsVisibleWhenPermitted()
    {
        verifyTabExistsWhenPermitted(OSGI_TAB);
    }

    @Test
    public void testOsgiTabIsHiddenWhenUnpermitted()
    {
        verifyTabDoesNotExistWhenUnpermitted(OSGI_TAB, GET_OSGI_STATE);
    }

    @Test
    public void testManagePluginModulesButtonsAreHiddenWhenUnpermissioned() throws IOException, JSONException
    {
        TestPlugins plugin = USER_INSTALLED_WITH_MODULES;
        try
        {
            addPermission(MANAGE_PLUGIN_INSTALL);
            restTester.installPluginsAndVerify(plugin);
            removePermission(MANAGE_PLUGIN_INSTALL);

            verifyElement().withClass("upm-module-actions").ofPlugin(plugin).onTab(MANAGE_EXISTING_TAB).isRemoved().whenPermission(MANAGE_PLUGIN_MODULE_ENABLEMENT).isDisallowed();
        }
        finally
        {
            addPermission(MANAGE_PLUGIN_UNINSTALL);
            restTester.uninstallPluginAndVerify(plugin);
            removePermission(MANAGE_PLUGIN_UNINSTALL);
        }
    }

    @Test
    public void testManagePluginButtonsAreHiddenWhenPluginKeyUnpermissioned()
        throws IOException, JSONException, InterruptedException
    {
        try
        {
            addPermission(MANAGE_PLUGIN_INSTALL);
            addPermission(MANAGE_PLUGIN_UNINSTALL);
            restTester.installPluginsAndVerify(USER_INSTALLED_WITH_MODULES);
            restTester.blacklistPluginPermissions(USER_INSTALLED_WITH_MODULES);

            uiTester.clickManageExistingTab();
            // We were already on manage tab
            uiTester.refreshPage();

            HtmlElement pluginDiv = getPluginDiv(MANAGE_EXISTING_TAB, USER_INSTALLED_WITH_MODULES);
            Doj.on(pluginDiv).get(".upm-plugin-row").click();
            uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();

            assertTrue(uiTester.currentPage().getById("upm-disable-test-plugin-v2-user-installed-with-modules").hasClass("hidden"));
        }
        finally
        {
            restTester.unblacklistPluginPermissions(USER_INSTALLED_WITH_MODULES);
            restTester.uninstallPluginAndVerify(USER_INSTALLED_WITH_MODULES);
            removePermission(MANAGE_PLUGIN_INSTALL);
            removePermission(MANAGE_PLUGIN_UNINSTALL);
        }
    }

    @Test
    public void testManagePluginModulesButtonsAreHiddenWhenModuleKeyUnpermissioned()
        throws IOException, JSONException, InterruptedException
    {
        String moduleKey = "some_module";
        try
        {
            addPermission(MANAGE_PLUGIN_UNINSTALL);
            addPermission(MANAGE_PLUGIN_INSTALL);
            restTester.installPluginsAndVerify(USER_INSTALLED_WITH_MODULES);
            restTester.blacklistModulePermissions(USER_INSTALLED_WITH_MODULES, moduleKey);

            uiTester.clickManageExistingTab();
            // We were already on manage tab
            uiTester.refreshPage();

            HtmlElement pluginDiv = getPluginDiv(MANAGE_EXISTING_TAB, USER_INSTALLED_WITH_MODULES);
            Doj.on(pluginDiv).get(".upm-plugin-row").click();
            uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();

            // Expand the modules
            Doj.on(pluginDiv).get(".upm-module-toggle").click();
            uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();

            // Assert that the module actions element has "cannot disable" class
            final Doj upmModuleActions = Doj.on(pluginDiv).getByClass("upm-module-actions");
            assertTrue(upmModuleActions.hasClass("upm-module-cannot-disable"));
        }
        finally
        {
            restTester.unblacklistModulePermissions(USER_INSTALLED_WITH_MODULES, moduleKey);
            restTester.uninstallPluginAndVerify(USER_INSTALLED_WITH_MODULES);
            removePermission(MANAGE_PLUGIN_INSTALL);
            removePermission(MANAGE_PLUGIN_UNINSTALL);
        }
    }

    @Test
    public void testPluginModulesAreHiddenWhenUnpermissioned() throws IOException, JSONException
    {
        TestPlugins plugin = USER_INSTALLED_WITH_MODULES;
        try
        {
            addPermission(MANAGE_PLUGIN_INSTALL);
            restTester.installPluginsAndVerify(plugin);
            removePermission(MANAGE_PLUGIN_INSTALL);

            verifyElement().withClass("upm-module-present").ofPlugin(plugin).onTab(MANAGE_EXISTING_TAB).isHidden().whenPermission(GET_PLUGIN_MODULES).isDisallowed();
        }
        finally
        {
            addPermission(MANAGE_PLUGIN_UNINSTALL);
            restTester.uninstallPluginAndVerify(plugin);
            removePermission(MANAGE_PLUGIN_UNINSTALL);
        }
    }

    @Test
    public void testManageAuditLogControlsAreHiddenWhenUnpermissioned() throws IOException
    {
        verifyElement().withId("upm-log-configure").onTab(AUDIT_LOG_TAB).isHidden().whenPermission(MANAGE_AUDIT_LOG).isDisallowed();
    }

    @Test
    public void testInstallButtonIsHiddenWhenUnpermissioned() throws IOException
    {
        verifyElement().withId("upm-upload").onTab(INSTALL_TAB).isHidden().whenPermission(MANAGE_PLUGIN_INSTALL).isDisallowed();
    }

    @Test
    public void testUpgradeAllButtonIsRemovedWhenUnpermissioned() throws IOException
    {
        verifyElement().withId("upm-upgrade-all").onTab(UPGRADE_TAB).isRemoved().whenPermission(MANAGE_PLUGIN_INSTALL).isDisallowed();
    }

    @Test
    public void testUpgradeButtonIsRemovedWhenUnpermissioned() throws IOException
    {
        verifyElement().withClass("upm-upgrade").ofPlugin(INSTALLABLE).onTab(UPGRADE_TAB).isRemoved().whenPermission(MANAGE_PLUGIN_INSTALL).isDisallowed();
    }

    @Test
    public void testUninstallButtonIsRemovedWhenUnpermissioned() throws IOException, JSONException
    {
        TestPlugins plugin = INSTALLABLE;
        try
        {
            addPermission(MANAGE_PLUGIN_INSTALL);
            restTester.installPluginsAndVerify(plugin);

            verifyElement().withId("upm-uninstall").ofPlugin(plugin).onTab(MANAGE_EXISTING_TAB).isRemoved().whenPermission(MANAGE_PLUGIN_UNINSTALL).isDisallowed();
        }
        finally
        {
            addPermission(MANAGE_PLUGIN_UNINSTALL);
            restTester.uninstallPluginAndVerify(plugin);
        }
    }

    @Test
    public void testPluginEnableButtonIsHiddenWhenUnpermissioned() throws IOException, JSONException, URISyntaxException
    {
        TestPlugins plugin = INSTALLABLE;
        try
        {
            addPermission(MANAGE_PLUGIN_INSTALL);
            restTester.installPluginsAndVerify(plugin);
            addPermission(MANAGE_PLUGIN_ENABLEMENT);
            restTester.disablePlugin(plugin);

            verifyElement().withId("upm-enable-" + plugin.getKey()).ofPlugin(plugin).onTab(MANAGE_EXISTING_TAB).isHidden().whenPermission(MANAGE_PLUGIN_ENABLEMENT).isDisallowed();
        }
        finally
        {
            addPermission(MANAGE_PLUGIN_UNINSTALL);
            restTester.uninstallPluginAndVerify(plugin);
        }
    }

    @Test
    public void testPluginDisableButtonIsHiddenWhenUnpermissioned() throws IOException, JSONException
    {
        TestPlugins plugin = INSTALLABLE;
        try
        {
            addPermission(MANAGE_PLUGIN_INSTALL);
            restTester.installPluginsAndVerify(plugin);

            verifyElement().withId("upm-disable-" + plugin.getKey()).ofPlugin(plugin).onTab(MANAGE_EXISTING_TAB).isHidden().whenPermission(MANAGE_PLUGIN_ENABLEMENT).isDisallowed();
        }
        finally
        {
            addPermission(MANAGE_PLUGIN_UNINSTALL);
            restTester.uninstallPluginAndVerify(plugin);
        }
    }

    @Test
    public void testEnterSafeModeLinkIsHiddenWhenUnpermissioned() throws IOException
    {
        verifyElement().withId("upm-safe-mode-on").onTab(MANAGE_EXISTING_TAB).isHidden().whenPermission(MANAGE_SAFE_MODE).isDisallowed();
    }

    @Test
    public void testExitSafeModeLinksAreHiddenWhenUnpermissioned() throws IOException
    {
        verifyElement().withId("upm-safe-mode-exit-links").onTab(MANAGE_EXISTING_TAB).isHidden().whenPermission(MANAGE_SAFE_MODE).isDisallowed();
    }

    /**
     * Ensures that the given tab is not hidden since the {@code Permission} is allowed.
     *
     * @param tab the tab
     */
    private void verifyTabExistsWhenPermitted(UpmTab tab)
    {
        reloadPage();
        assertTrue(uiTester.isUserPermittedToViewTab(tab));
    }

    /**
     * Ensures that the given tab is hidden because it is unpermitted to view it.
     *
     * @param tab the tab
     * @param permission the {@code Permission} required to view the tab
     */
    private void verifyTabDoesNotExistWhenUnpermitted(UpmTab tab, Permission permission)
    {
        try
        {
            removePermission(permission);
            reloadPage();

            assertFalse(uiTester.isUserPermittedToViewTab(tab));
        }
        finally
        {
            addPermission(permission);
        }
    }

    private static void reloadPage()
    {
        uiTester.refreshPage();
        uiTester.goToUpmPage();
    }

    private static void addPermission(Permission permission)
    {
        restTester.addPermission(permission);
    }

    private static void removePermission(Permission permission)
    {
        restTester.removePermission(permission);
    }

    /**
     * Returns a new {@code PermissionedUiTester} to run a permissioned UI test case.
     *
     * @return a new {@code PermissionedUiTester} to run a permissioned UI test case.
     */
    private static PermissionedUiTester verifyElement()
    {
        return new PermissionedUiTester();
    }

    /**
     * An attempt at making these tests more readable due to the multiple number of parameters per invocation.
     */
    private static class PermissionedUiTester
    {
        private String value;
        private String attribute;
        private UpmTab tab;
        private TestPlugins plugin;
        private Permission permission;
        private Boolean hidden;

        PermissionedUiTester withId(String value)
        {
            this.attribute = "id";
            this.value = value;
            return this;
        }

        PermissionedUiTester withClass(String value)
        {
            this.attribute = "class";
            this.value = value;
            return this;
        }

        PermissionedUiTester ofPlugin(TestPlugins plugin)
        {
            this.plugin = plugin;
            return this;
        }

        PermissionedUiTester isHidden()
        {
            hidden = true;
            return this;
        }

        PermissionedUiTester isRemoved()
        {
            hidden = false;
            return this;
        }

        PermissionedUiTester whenPermission(Permission permission)
        {
            this.permission = permission;
            return this;
        }

        PermissionedUiTester onTab(UpmTab tab)
        {
            this.tab = tab;
            return this;
        }

        /**
         * Executes the actual test
         */
        void isDisallowed() throws IOException
        {
            if (hidden)
            {
                verifyElementIsHiddenWhenPermissionIsDisallowed(permission, attribute, value, tab, plugin);
            }
            else
            {
                verifyElementIsRemovedWhenPermissionIsDisallowed(permission, attribute, value, tab, plugin);
            }
        }

        /**
         * Ensures that the given element is hidden when the given {@code Permission} is disallowed but all others are allowed.
         *
         * @param permission the {@code Permission} to disallow
         * @param attribute the attribute in the element which will be checked
         * @param elementAttribute the element which should be hidden
         * @param tab the tab on which the element exists
         * @param plugin the plugin which needs to be expanded, or null if none need to be expanded
         * @throws IOException when clicking the plugin div
         */
        private static void verifyElementIsHiddenWhenPermissionIsDisallowed(Permission permission, String attribute, String elementAttribute, UpmTab tab, TestPlugins plugin) throws IOException
        {
            try
            {
                verifyElementHasChangedPrep(permission, tab, plugin);
                assertThat(uiTester.currentPage().getByAttribute(attribute, MatchType.CONTAINING, elementAttribute).classValues()[0], containsString("hidden"));
            }
            finally
            {
                addPermission(permission);
            }
        }

        /**
         * Ensures that the given element is hidden when the given {@code Permission} is disallowed but all others are allowed.
         *
         * @param permission the {@code Permission} to disallow
         * @param attribute the attribute in the element which will be checked
         * @param elementAttribute the element which should be hidden
         * @param tab the tab on which the element exists
         * @param plugin the plugin which needs to be expanded, or null if none need to be expanded
         * @throws IOException when clicking the plugin div
         */
        private static void verifyElementIsRemovedWhenPermissionIsDisallowed(Permission permission, String attribute, String elementAttribute, UpmTab tab, TestPlugins plugin) throws IOException
        {
            try
            {
                verifyElementHasChangedPrep(permission, tab, plugin);
                assertThat(uiTester.currentPage().getByAttribute(attribute, elementAttribute), is(equalTo(Doj.EMPTY)));
            }
            finally
            {
                addPermission(permission);
            }
        }

        /**
         * Setting up a test case for verifying that an element has either been removed or hid due to a permission being disallowed.
         *
         * @param permission the {@code Permission} which is disallowed
         * @param tab the tab on which the element exists
         * @param plugin the plugin in which the element exists, or null if the element is not within a plugin div
         * @throws IOException when clicking the plugin div
         */
        private static void verifyElementHasChangedPrep(Permission permission, UpmTab tab, TestPlugins plugin) throws IOException
        {
            removePermission(permission);
            reloadPage();

            uiTester.clickTab(tab);

            if (plugin != null)
            {
                //expand plugin
                HtmlElement pluginDiv = getPluginDiv(tab, plugin);
                Doj.on(pluginDiv).get(".upm-plugin-row").click();
                uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
            }
        }

    }

    private static HtmlElement getPluginDiv(UpmTab tab, TestPlugins plugin)
    {
        URI pluginUri;
        if (tab.equals(UPGRADE_TAB))
        {
            pluginUri = uriBuilder.buildAvailablePluginUri(plugin.getKey());
        }
        else
        {
            pluginUri = uriBuilder.buildPluginUri(plugin.getKey());
        }
        return uiTester.elementById(uiTester.getTabPanelId(tab)).get("input").withValue(pluginUri.toString()).parent().firstElement();
    }


}
