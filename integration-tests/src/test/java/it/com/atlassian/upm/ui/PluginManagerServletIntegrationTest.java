package it.com.atlassian.upm.ui;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.test.UpmUiTestBase;

import org.junit.Ignore;
import org.junit.Test;

import static com.atlassian.upm.test.UpmTestGroups.REFAPP;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PluginManagerServletIntegrationTest extends UpmUiTestBase
{
    @Test
    public void assertThatUserIsRedirectedToLoginPageWhenNotLoggedIn()
    {
        uiTester.goToUpmPage();
        assertTrue(uiTester.isOnLoginPage());
    }

    @Test
    @TestGroups(value = REFAPP, reason = "Only Refapp has the Barney user")
    public void assertThatUserIsRedirectedToLoginPageWhenLoggedInButNotAnAdmin()
    {
        uiTester.logInAs("barney");
        try
        {
            uiTester.goToUpmPage();
            assertTrue(uiTester.isOnLoginPage());
        }
        finally
        {
            uiTester.logout();
        }
    }

    @Test
    @Ignore("UPM-819 - bump refapp version. New user was not picked up at 2.7.0-alpha7")
    @TestGroups(value = REFAPP, reason = "Only Refapp has the Betty user")
    public void assertThatUserIsNotRedirectedWhenLoggedInAsAnAdmin()
    {
        uiTester.logInAs("betty");
        try
        {
            uiTester.goToUpmPage();
            assertFalse(uiTester.isOnLoginPage());
        }
        finally
        {
            uiTester.logout();
        }
    }

    @Test
    public void assertThatUserIsNotRedirectedWhenLoggedInAsASystemAdmin()
    {
        uiTester.logInAs("admin");
        try
        {
            uiTester.goToUpmPage();
            assertFalse(uiTester.isOnLoginPage());
        }
        finally
        {
            uiTester.logout();
        }
    }
}
