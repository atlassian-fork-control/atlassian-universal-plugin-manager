package it.com.atlassian.upm.rest.resources;

import com.atlassian.plugin.PluginRestartState;
import com.atlassian.upm.rest.representations.ChangesRequiringRestartRepresentation;
import com.atlassian.upm.rest.representations.ChangesRequiringRestartRepresentation.ChangeRepresentation;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmResourceTestBase;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Test;

import static com.atlassian.plugin.PluginRestartState.INSTALL;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_REQUIRES_RESTART;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class ChangeRequiringRestartCollectionResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatInstallingPluginRequiringRestartShowsUpInRequiresRestartResource() throws Exception
    {
        restTester.installPluginAndWaitForCompletion(INSTALLABLE_REQUIRES_RESTART);
        try
        {
            ChangesRequiringRestartRepresentation changes = restTester.getChangesRequiringRestart();
            assertThat(changes.getChanges(), contains(change(INSTALLABLE_REQUIRES_RESTART, INSTALL)));
        }
        finally
        {
            restTester.deleteChangeRequiringRestart(INSTALLABLE_REQUIRES_RESTART);
        }
    }

    @Test
    public void assertThatNoPluginsRequiringRestartShowUpInRequiresRestartResourceWhenNoneHaveBeenInstalledOrUpgraded() throws Exception
    {
        ChangesRequiringRestartRepresentation changes = restTester.getChangesRequiringRestart();
        assertThat(changes.getChanges(), Matchers.<ChangeRepresentation>emptyIterable());
    }

    private Matcher<ChangeRepresentation> change(final TestPlugins plugin, final PluginRestartState restartState)
    {
        return new TypeSafeDiagnosingMatcher<ChangeRepresentation>()
        {
            @Override
            protected boolean matchesSafely(ChangeRepresentation changeRepresentation, Description mismatchDescription)
            {
                if (!plugin.getName().equals(changeRepresentation.getName()) || !restartState.toString().toLowerCase().equals(
                    changeRepresentation.getAction()))
                {
                    mismatchDescription.appendValue(changeRepresentation.getAction()).appendText(" of ").appendValue(
                        changeRepresentation.getName());
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendValue(restartState.toString().toLowerCase()).appendText(" of ").appendValue(plugin.getName());
            }
        };
    }
}
